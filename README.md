# Installation guide
1. git clone https://bitbucket.org/regentix/eindwerk-backend
2. cd eindwerk-backend
3. composer install
4. cp .env.local .env
5. configure .env
6. php bin/console d:s:u --force