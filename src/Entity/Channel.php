<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ApiResource()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ChannelRepository")
 */
class Channel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelUser", mappedBy="channel", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id", nullable=true)
     */
    private $channelUser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ChannelGroup", inversedBy="channel")
     * @ORM\JoinColumn(name="channel_group_id", referencedColumnName="id", nullable=true)
     */
    private $channelGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpace", inversedBy="channel")
     */
    private $workSpace;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpaceRole", inversedBy="channels")
     */
    private $baseRole;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module", mappedBy="channel", cascade={"persist", "remove"})
     */
    private $modules;

    public function __construct()
    {
        $this->channelUser = new ArrayCollection();
        $this->modules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getChannelUser(): Collection
    {
        return $this->channelUser;
    }

    public function addChannelUser(ChannelUser $channelUser): self
    {
        if (!$this->channelUser->contains($channelUser)) {
            $this->channelUser[] = $channelUser;
            $channelUser->setChannel($this);
        }

        return $this;
    }

    public function removeChannelUser(ChannelUser $channelUser): self
    {
        if ($this->channelUser->contains($channelUser)) {
            $this->channelUser->removeElement($channelUser);
            // set the owning side to null (unless already changed)
            if ($channelUser->getChannel() === $this) {
                $channelUser->setChannel(null);
            }
        }

        return $this;
    }

    public function getChannelGroup(): ?ChannelGroup
    {
        return $this->channelGroup;
    }

    public function setChannelGroup(?ChannelGroup $channelGroup): self
    {
        $this->channelGroup = $channelGroup;

        return $this;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getBaseRole(): ?WorkSpaceRole
    {
        return $this->baseRole;
    }

    public function setBaseRole(?WorkSpaceRole $baseRole): self
    {
        $this->baseRole = $baseRole;

        return $this;
    }

    /**
     * @return Collection|Module[]
     */
    public function getModules(): Collection
    {
        return $this->modules;
    }

    public function addModule(Module $module): self
    {
        if (!$this->modules->contains($module)) {
            $this->modules[] = $module;
            $module->setChannel($this);
        }

        return $this;
    }

    public function removeModule(Module $module): self
    {
        if ($this->modules->contains($module)) {
            $this->modules->removeElement($module);
            // set the owning side to null (unless already changed)
            if ($module->getChannel() === $this) {
                $module->setChannel(null);
            }
        }

        return $this;
    }
}
