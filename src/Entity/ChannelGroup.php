<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ChannelGroupRepository")
 */
class ChannelGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Channel", mappedBy="channelGroup", cascade={"persist", "remove"})
     */
    private $channel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpace", inversedBy="channelGroup")
     */
    private $workSpace;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelGroupUser", mappedBy="channelGroup", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="channel_group_id", referencedColumnName="id", nullable=true)
     */
    private $channelGroupUsers;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpaceRole", inversedBy="channelGroups")
     */
    private $baseRole;

    public function __construct()
    {
        $this->channel = new ArrayCollection();
        $this->channelGroupUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Channel[]
     */
    public function getChannel(): Collection
    {
        return $this->channel;
    }

    public function addChannel(Channel $channel): self
    {
        if (!$this->channel->contains($channel)) {
            $this->channel[] = $channel;
            $channel->setChannelGroup($this);
        }

        return $this;
    }

    public function removeChannel(Channel $channel): self
    {
        if ($this->channel->contains($channel)) {
            $this->channel->removeElement($channel);
            // set the owning side to null (unless already changed)
            if ($channel->getChannelGroup() === $this) {
                $channel->setChannelGroup(null);
            }
        }

        return $this;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getChannelGroupUsers(): Collection
    {
        return $this->channelGroupUsers;
    }

    public function addChannelGroupUser(ChannelGroupUser $channelGroupUser): self
    {
        if (!$this->channelGroupUsers->contains($channelGroupUser)) {
            $this->channelGroupUsers[] = $channelGroupUser;
            $channelGroupUser->setChannelGroup($this);
        }

        return $this;
    }

    public function removeChannelGroupUser(ChannelGroupUser $channelGroupUser): self
    {
        if ($this->channelGroupUsers->contains($channelGroupUser)) {
            $this->channelGroupUsers->removeElement($channelGroupUser);
            // set the owning side to null (unless already changed)
            if ($channelGroupUser->getChannelGroup() === $this) {
                $channelGroupUser->setChannelGroup(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBaseRole(): ?WorkSpaceRole
    {
        return $this->baseRole;
    }

    public function setBaseRole(?WorkSpaceRole $baseRole): self
    {
        $this->baseRole = $baseRole;

        return $this;
    }
}
