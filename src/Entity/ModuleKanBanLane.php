<?php

namespace App\Entity;

use Doctrine\Common\Collections\Criteria;
use JsonSerializable;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ModuleKanBanLaneRepository")
 */
class ModuleKanBanLane implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleKanBanData", mappedBy="moduleKanBanLane", cascade={"persist","remove"})
     */
    private $data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module", inversedBy="moduleKanBanLane")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module;

    /**
     * @ORM\Column(type="integer")
     */
    private $colOrder;

    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return array(
            'id'     => (string) $this->getId(),
            'title'  => $this->getTitle(),
            'cards'  => $this->getDataOrderedByRoleOrder()
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDataOrderedByRoleOrder()
    {
        $criteria = Criteria::create()->orderBy(['rowOrder' => 'ASC']);
        return $this->getData()->matching($criteria)->toArray();
    }

    /**
     * @return Collection|ModuleKanBanData[]
     */
    public function getData(): Collection
    {
        return $this->data;
    }

    public function addData(ModuleKanBanData $data): self
    {
        if (!$this->data->contains($data)) {
            $this->data[] = $data;
            $data->setModuleKanBanLane($this);
        }

        return $this;
    }

    public function removeData(ModuleKanBanData $data): self
    {
        if ($this->data->contains($data)) {
            $this->data->removeElement($data);
            // set the owning side to null (unless already changed)
            if ($data->getModuleKanBanLane() === $this) {
                $data->setModuleKanBanLane(null);
            }
        }

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getColOrder(): ?int
    {
        return $this->colOrder;
    }

    public function setColOrder(int $colOrder): self
    {
        $this->colOrder = $colOrder;

        return $this;
    }
}
