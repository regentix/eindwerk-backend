<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity()
 */
class WorkSpaceUserRole
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpaceRole", inversedBy="workSpaceUserRoles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $workSpaceRole;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpaceUser", inversedBy="workSpaceUserRoles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $workSpaceUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWorkSpaceRole(): ?WorkSpaceRole
    {
        return $this->workSpaceRole;
    }

    public function setWorkSpaceRole(?WorkSpaceRole $workSpaceRole): self
    {
        $this->workSpaceRole = $workSpaceRole;

        return $this;
    }

    public function getWorkSpaceUser(): ?WorkSpaceUser
    {
        return $this->workSpaceUser;
    }

    public function setWorkSpaceUser(?WorkSpaceUser $workSpaceUser): self
    {
        $this->workSpaceUser = $workSpaceUser;

        return $this;
    }
}
