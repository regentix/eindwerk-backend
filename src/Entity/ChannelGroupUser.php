<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity()
 */
class ChannelGroupUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="channelGroupUsers")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ChannelGroup", inversedBy="channelGroupUsers")
     */
    private $channelGroup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getChannelGroup(): ?ChannelGroup
    {
        return $this->channelGroup;
    }

    public function setChannelGroup(?ChannelGroup $channelGroup): self
    {
        $this->channelGroup = $channelGroup;

        return $this;
    }
}
