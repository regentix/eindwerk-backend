<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"email"})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ResetToken", mappedBy="user", cascade={"persist", "remove"})
     */
    private $resetToken;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceUser", mappedBy="user", cascade={"persist", "remove"})
     */
    private $workSpaceUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelGroupUser", mappedBy="user", cascade={"persist", "remove"})
     */
    private $channelGroupUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelUser", mappedBy="user", cascade={"persist", "remove"})
     */
    private $channelUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserNotification", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userNotifications;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceInvite", mappedBy="sentBy", cascade={"persist", "remove"})
     */
    private $workSpaceInvites;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status = "online";

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleChatData", mappedBy="sentBy", cascade={"persist", "remove"})
     */
    private $moduleChatData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleTimeTrackData", mappedBy="user", cascade={"persist", "remove"})
     */
    private $moduleTimeTrackData;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleKanBanData", mappedBy="user")
     */
    private $moduleKanBanData;

    public function __construct()
    {
        $this->workSpaceUsers = new ArrayCollection();
        $this->channelGroupUsers = new ArrayCollection();
        $this->userNotifications = new ArrayCollection();
        $this->workSpaceInvites = new ArrayCollection();
        $this->moduleChatData = new ArrayCollection();
        $this->moduleTimeTrackData = new ArrayCollection();
        $this->channelUsers = new ArrayCollection();
        $this->moduleKanBanData = new ArrayCollection();
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return (string) $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getResetToken(): ?ResetToken
    {
        return $this->resetToken;
    }

    public function setResetToken(?ResetToken $resetToken): self
    {
        $this->resetToken = $resetToken;

        // set (or unset) the owning side of the relation if necessary
        $newUser = $resetToken === null ? null : $this;
        if ($newUser !== $resetToken->getUser()) {
            $resetToken->setUser($newUser);
        }

        return $this;
    }

    public function getWorkSpaceUsers(): Collection
    {
        return $this->workSpaceUsers;
    }

    public function addWorkSpaceUser(WorkSpaceUser $workSpaceUser): self
    {
        if (!$this->workSpaceUsers->contains($workSpaceUser)) {
            $this->workSpaceUsers[] = $workSpaceUser;
            $workSpaceUser->setUser($this);
        }

        return $this;
    }

    public function removeWorkSpaceUser(WorkSpaceUser $workSpaceUser): self
    {
        if ($this->workSpaceUsers->contains($workSpaceUser)) {
            $this->workSpaceUsers->removeElement($workSpaceUser);
            // set the owning side to null (unless already changed)
            if ($workSpaceUser->getUser() === $this) {
                $workSpaceUser->setUser(null);
            }
        }

        return $this;
    }

    public function getChannelGroupUsers(): Collection
    {
        return $this->channelGroupUsers;
    }

    public function addChannelGroupUser(ChannelGroupUser $channelGroupUser): self
    {
        if (!$this->channelGroupUsers->contains($channelGroupUser)) {
            $this->channelGroupUsers[] = $channelGroupUser;
            $channelGroupUser->setUser($this);
        }

        return $this;
    }

    public function removeChannelGroupUser(ChannelGroupUser $channelGroupUser): self
    {
        if ($this->channelGroupUsers->contains($channelGroupUser)) {
            $this->channelGroupUsers->removeElement($channelGroupUser);
            // set the owning side to null (unless already changed)
            if ($channelGroupUser->getUser() === $this) {
                $channelGroupUser->setUser(null);
            }
        }

        return $this;
    }

    public function getUserNotifications(): Collection
    {
        return $this->userNotifications;
    }

    public function addUserNotification(UserNotification $userNotification): self
    {
        if (!$this->userNotifications->contains($userNotification)) {
            $this->userNotifications[] = $userNotification;
            $userNotification->setUser($this);
        }

        return $this;
    }

    public function removeUserNotification(UserNotification $userNotification): self
    {
        if ($this->userNotifications->contains($userNotification)) {
            $this->userNotifications->removeElement($userNotification);
            // set the owning side to null (unless already changed)
            if ($userNotification->getUser() === $this) {
                $userNotification->setUser(null);
            }
        }

        return $this;
    }

    public function getWorkSpaceInvites(): Collection
    {
        return $this->workSpaceInvites;
    }

    public function addWorkSpaceInvite(WorkSpaceInvite $workSpaceInvite): self
    {
        if (!$this->workSpaceInvites->contains($workSpaceInvite)) {
            $this->workSpaceInvites[] = $workSpaceInvite;
            $workSpaceInvite->setTarget($this);
        }

        return $this;
    }

    public function removeWorkSpaceInvite(WorkSpaceInvite $workSpaceInvite): self
    {
        if ($this->workSpaceInvites->contains($workSpaceInvite)) {
            $this->workSpaceInvites->removeElement($workSpaceInvite);
            // set the owning side to null (unless already changed)
            if ($workSpaceInvite->getTarget() === $this) {
                $workSpaceInvite->setTarget(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ModuleChatData[]
     */
    public function getModuleChatData(): Collection
    {
        return $this->moduleChatData;
    }

    public function addModuleChatData(ModuleChatData $moduleChatData): self
    {
        if (!$this->moduleChatData->contains($moduleChatData)) {
            $this->moduleChatData[] = $moduleChatData;
            $moduleChatData->setSentBy($this);
        }

        return $this;
    }

    public function removeModuleChatData(ModuleChatData $moduleChatData): self
    {
        if ($this->moduleChatData->contains($moduleChatData)) {
            $this->moduleChatData->removeElement($moduleChatData);
            // set the owning side to null (unless already changed)
            if ($moduleChatData->getSentBy() === $this) {
                $moduleChatData->setSentBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ModuleTimeTrackData[]
     */
    public function getModuleTimeTrackData(): Collection
    {
        return $this->moduleTimeTrackData;
    }

    public function addModuleTimeTrackData(ModuleTimeTrackData $moduleTimeTrackData): self
    {
        if (!$this->moduleTimeTrackData->contains($moduleTimeTrackData)) {
            $this->moduleTimeTrackData[] = $moduleTimeTrackData;
            $moduleTimeTrackData->setUser($this);
        }

        return $this;
    }

    public function removeModuleTimeTrackData(ModuleTimeTrackData $moduleTimeTrackData): self
    {
        if ($this->moduleTimeTrackData->contains($moduleTimeTrackData)) {
            $this->moduleTimeTrackData->removeElement($moduleTimeTrackData);
            // set the owning side to null (unless already changed)
            if ($moduleTimeTrackData->getUser() === $this) {
                $moduleTimeTrackData->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ChannelUser[]
     */
    public function getChannelUsers(): Collection
    {
        return $this->channelUsers;
    }

    public function addChannelUser(ChannelUser $channelUser): self
    {
        if (!$this->channelUsers->contains($channelUser)) {
            $this->channelUsers[] = $channelUser;
            $channelUser->setUser($this);
        }

        return $this;
    }

    public function removeChannelUser(ChannelUser $channelUser): self
    {
        if ($this->channelUsers->contains($channelUser)) {
            $this->channelUsers->removeElement($channelUser);
            // set the owning side to null (unless already changed)
            if ($channelUser->getUser() === $this) {
                $channelUser->setUser(null);
            }
        }

        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return Collection|ModuleKanBanData[]
     */
    public function getModuleKanBanData(): Collection
    {
        return $this->moduleKanBanData;
    }

    public function addModuleKanBanData(ModuleKanBanData $moduleKanBanData): self
    {
        if (!$this->moduleKanBanData->contains($moduleKanBanData)) {
            $this->moduleKanBanData[] = $moduleKanBanData;
            $moduleKanBanData->setUser($this);
        }

        return $this;
    }

    public function removeModuleKanBanData(ModuleKanBanData $moduleKanBanData): self
    {
        if ($this->moduleKanBanData->contains($moduleKanBanData)) {
            $this->moduleKanBanData->removeElement($moduleKanBanData);
            // set the owning side to null (unless already changed)
            if ($moduleKanBanData->getUser() === $this) {
                $moduleKanBanData->setUser(null);
            }
        }

        return $this;
    }
}
