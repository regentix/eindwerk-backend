<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ResetToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="resetToken", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expires_at;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ConfirmationToken", mappedBy="resetToken", cascade={"persist", "remove"})
     */
    private $confirmationToken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expires_at;
    }

    public function setExpiresAt(\DateTimeInterface $expires_at): self
    {
        $this->expires_at = $expires_at;

        return $this;
    }

    public function getConfirmationToken(): ?ConfirmationToken
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(ConfirmationToken $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        // set the owning side of the relation if necessary
        if ($this !== $confirmationToken->getResetToken()) {
            $confirmationToken->setResetToken($this);
        }

        return $this;
    }
}
