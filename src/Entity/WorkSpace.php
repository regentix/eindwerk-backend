<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource(
 *     itemOperations = {
 *         "get"    = {"method"="GET",      "access_control"="is_granted('WORKSPACE_VIEW', object)"},
 *         "put"    = {"method"="PUT",      "access_control"="is_granted('WORKSPACE_EDIT', object)"},
 *         "delete" = {"method"="DELETE",   "access_control"="is_granted('WORKSPACE_DELETE', object)"},
 *     },
 *     collectionOperations = {
 *         "get"
 *     })
 * @ApiFilter(SearchFilter::class, properties={"name"})
 * @ApiFilter(OrderFilter::class, properties={"workSpaceLogs.id"}, arguments={"orderParameterName"="order"}))
 * @ORM\Entity(repositoryClass="App\Repository\WorkSpaceRepository")
 * @UniqueEntity(
 *     fields  = {"name"},
 *     message = "This name has already been taken."
 * )
 */
class WorkSpace
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Channel", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $channel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelGroup", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $channelGroup;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceUser", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $workSpaceUsers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @var MediaObject|null
     *
     * @ORM\OneToOne(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     */
    public $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceInvite", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $workSpaceInvites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceLog", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $workSpaceLogs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceRole", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $workSpaceRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module", mappedBy="workSpace", cascade={"persist","remove"})
     */
    private $modules;

    public function __construct()
    {
        $this->channel = new ArrayCollection();
        $this->channelGroup = new ArrayCollection();
        $this->workSpaceUsers = new ArrayCollection();
        $this->workSpaceInvites = new ArrayCollection();
        $this->workSpaceLogs = new ArrayCollection();
        $this->workSpaceRoles = new ArrayCollection();
        $this->modules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChannel(): Collection
    {
        return $this->channel;
    }

    public function addChannel(Channel $channel): self
    {
        if (!$this->channel->contains($channel)) {
            $this->channel[] = $channel;
            $channel->setWorkSpace($this);
        }

        return $this;
    }

    public function removeChannel(Channel $channel): self
    {
        if ($this->channel->contains($channel)) {
            $this->channel->removeElement($channel);
            // set the owning side to null (unless already changed)
            if ($channel->getWorkSpace() === $this) {
                $channel->setWorkSpace(null);
            }
        }

        return $this;
    }

    public function getChannelGroup(): Collection
    {
        return $this->channelGroup;
    }

    public function addChannelGroup(ChannelGroup $channelGroup): self
    {
        if (!$this->channelGroup->contains($channelGroup)) {
            $this->channelGroup[] = $channelGroup;
            $channelGroup->setWorkSpace($this);
        }

        return $this;
    }

    public function removeChannelGroup(ChannelGroup $channelGroup): self
    {
        if ($this->channelGroup->contains($channelGroup)) {
            $this->channelGroup->removeElement($channelGroup);
            // set the owning side to null (unless already changed)
            if ($channelGroup->getWorkSpace() === $this) {
                $channelGroup->setWorkSpace(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWorkSpaceUsers(): Collection
    {
        return $this->workSpaceUsers;
    }

    public function addWorkSpaceUser(WorkSpaceUser $workSpaceUser): self
    {
        if (!$this->workSpaceUsers->contains($workSpaceUser)) {
            $this->workSpaceUsers[] = $workSpaceUser;
            $workSpaceUser->setWorkspace($this);
        }

        return $this;
    }

    public function removeWorkSpaceUser(WorkSpaceUser $workSpaceUser): self
    {
        if ($this->workSpaceUsers->contains($workSpaceUser)) {
            $this->workSpaceUsers->removeElement($workSpaceUser);
            // set the owning side to null (unless already changed)
            if ($workSpaceUser->getWorkspace() === $this) {
                $workSpaceUser->setWorkspace(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getWorkSpaceInvites(): Collection
    {
        return $this->workSpaceInvites;
    }

    public function addWorkSpaceInvite(WorkSpaceInvite $workSpaceInvite): self
    {
        if (!$this->workSpaceInvites->contains($workSpaceInvite)) {
            $this->workSpaceInvites[] = $workSpaceInvite;
            $workSpaceInvite->setWorkSpace($this);
        }

        return $this;
    }

    public function removeWorkSpaceInvite(WorkSpaceInvite $workSpaceInvite): self
    {
        if ($this->workSpaceInvites->contains($workSpaceInvite)) {
            $this->workSpaceInvites->removeElement($workSpaceInvite);
            // set the owning side to null (unless already changed)
            if ($workSpaceInvite->getWorkSpace() === $this) {
                $workSpaceInvite->setWorkSpace(null);
            }
        }

        return $this;
    }

    public function getWorkSpaceLogsOrderedById()
    {
        $criteria = Criteria::create()->orderBy(['id' => 'DESC'])->setMaxResults(30);
        return $this->getWorkSpaceLogs()->matching($criteria)->toArray();
    }

    public function getWorkSpaceLogs(): Collection
    {
        return $this->workSpaceLogs;
    }

    public function addWorkSpaceLog(WorkSpaceLog $workSpaceLog): self
    {
        if (!$this->workSpaceLogs->contains($workSpaceLog)) {
            $this->workSpaceLogs[] = $workSpaceLog;
            $workSpaceLog->setWorkSpace($this);
        }

        return $this;
    }

    public function removeWorkSpaceLog(WorkSpaceLog $workSpaceLog): self
    {
        if ($this->workSpaceLogs->contains($workSpaceLog)) {
            $this->workSpaceLogs->removeElement($workSpaceLog);
            // set the owning side to null (unless already changed)
            if ($workSpaceLog->getWorkSpace() === $this) {
                $workSpaceLog->setWorkSpace(null);
            }
        }

        return $this;
    }

    public function getWorkSpaceRoles(): Collection
    {
        return $this->workSpaceRoles;
    }

    public function addWorkSpaceRole(WorkSpaceRole $workSpaceRole): self
    {
        if (!$this->workSpaceRoles->contains($workSpaceRole)) {
            $this->workSpaceRoles[] = $workSpaceRole;
            $workSpaceRole->setWorkSpace($this);
        }

        return $this;
    }

    public function removeWorkSpaceRole(WorkSpaceRole $workSpaceRole): self
    {
        if ($this->workSpaceRoles->contains($workSpaceRole)) {
            $this->workSpaceRoles->removeElement($workSpaceRole);
            // set the owning side to null (unless already changed)
            if ($workSpaceRole->getWorkSpace() === $this) {
                $workSpaceRole->setWorkSpace(null);
            }
        }

        return $this;
    }

    public function getModules(): Collection
    {
        return $this->modules;
    }

    public function addModule(Module $module): self
    {
        if (!$this->modules->contains($module)) {
            $this->modules[] = $module;
            $module->setWorkSpace($this);
        }

        return $this;
    }

    public function removeModule(Module $module): self
    {
        if ($this->modules->contains($module)) {
            $this->modules->removeElement($module);
            // set the owning side to null (unless already changed)
            if ($module->getWorkSpace() === $this) {
                $module->setWorkSpace(null);
            }
        }

        return $this;
    }
}
