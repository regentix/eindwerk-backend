<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity()
 */
class WorkSpaceRole
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpace", inversedBy="workSpaceRoles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $workSpace;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    /**
     * @ORM\Column(type="integer")
     */
    private $roleOrder;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceUserRole", mappedBy="workSpaceRole", cascade={"persist","remove"})
     */
    private $workSpaceUserRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Channel", mappedBy="baseRole")
     */
    private $channels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelGroup", mappedBy="baseRole")
     */
    private $channelGroups;

    public function __construct()
    {
        $this->workSpaceUserRoles = new ArrayCollection();
        $this->channels = new ArrayCollection();
        $this->channelGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getRoleOrder(): ?int
    {
        return $this->roleOrder;
    }

    public function setRoleOrder(int $roleOrder): self
    {
        $this->roleOrder = $roleOrder;

        return $this;
    }

    public function getWorkSpaceUserRoles(): Collection
    {
        return $this->workSpaceUserRoles;
    }

    public function addWorkSpaceUserRole(WorkSpaceUserRole $workSpaceUserRole): self
    {
        if (!$this->workSpaceUserRoles->contains($workSpaceUserRole)) {
            $this->workSpaceUserRoles[] = $workSpaceUserRole;
            $workSpaceUserRole->setWorkSpaceRole($this);
        }

        return $this;
    }

    public function removeWorkSpaceUserRole(WorkSpaceUserRole $workSpaceUserRole): self
    {
        if ($this->workSpaceUserRoles->contains($workSpaceUserRole)) {
            $this->workSpaceUserRoles->removeElement($workSpaceUserRole);
            // set the owning side to null (unless already changed)
            if ($workSpaceUserRole->getWorkSpaceRole() === $this) {
                $workSpaceUserRole->setWorkSpaceRole(null);
            }
        }

        return $this;
    }

    public function getChannels(): Collection
    {
        return $this->channels;
    }

    public function addChannel(Channel $channel): self
    {
        if (!$this->channels->contains($channel)) {
            $this->channels[] = $channel;
            $channel->setBaseRole($this);
        }

        return $this;
    }

    public function removeChannel(Channel $channel): self
    {
        if ($this->channels->contains($channel)) {
            $this->channels->removeElement($channel);
            // set the owning side to null (unless already changed)
            if ($channel->getBaseRole() === $this) {
                $channel->setBaseRole(null);
            }
        }

        return $this;
    }

    public function getChannelGroups(): Collection
    {
        return $this->channelGroups;
    }

    public function addChannelGroup(ChannelGroup $channelGroup): self
    {
        if (!$this->channelGroups->contains($channelGroup)) {
            $this->channelGroups[] = $channelGroup;
            $channelGroup->setBaseRole($this);
        }

        return $this;
    }

    public function removeChannelGroup(ChannelGroup $channelGroup): self
    {
        if ($this->channelGroups->contains($channelGroup)) {
            $this->channelGroups->removeElement($channelGroup);
            // set the owning side to null (unless already changed)
            if ($channelGroup->getBaseRole() === $this) {
                $channelGroup->setBaseRole(null);
            }
        }

        return $this;
    }
}
