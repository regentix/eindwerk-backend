<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"user", "workSpace.name"})
 * @ORM\Entity(repositoryClass="App\Repository\WorkSpaceUserRepository")
 */
class WorkSpaceUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="workSpaceUsers")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpace", inversedBy="workSpaceUsers")
     */
    private $workSpace;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkSpaceUserRole", mappedBy="workSpaceUser", cascade={"persist","remove"})
     */
    private $workSpaceUserRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleTimeTrackData", mappedBy="workSpaceUser", cascade={"persist","remove"})
     */
    private $trackedTime;

    public function __construct()
    {
        $this->workSpaceUserRoles = new ArrayCollection();
        $this->trackedTime = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workspace): self
    {
        $this->workSpace = $workspace;

        return $this;
    }

    public function getWorkSpaceUserRole()
    {
        return $this->workSpaceUserRoles[0];
    }

    public function getWorkSpaceUserRoles(): Collection
    {
        return $this->workSpaceUserRoles;
    }

    public function addWorkSpaceUserRole(WorkSpaceUserRole $workSpaceUserRole): self
    {
        if (!$this->workSpaceUserRoles->contains($workSpaceUserRole)) {
            $this->workSpaceUserRoles[] = $workSpaceUserRole;
            $workSpaceUserRole->setWorkSpaceUser($this);
        }

        return $this;
    }

    public function removeWorkSpaceUserRole(WorkSpaceUserRole $workSpaceUserRole): self
    {
        if ($this->workSpaceUserRoles->contains($workSpaceUserRole)) {
            $this->workSpaceUserRoles->removeElement($workSpaceUserRole);
            // set the owning side to null (unless already changed)
            if ($workSpaceUserRole->getWorkSpaceUser() === $this) {
                $workSpaceUserRole->setWorkSpaceUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ModuleTimeTrackData[]
     */
    public function getTrackedTime(): Collection
    {
        return $this->trackedTime;
    }

    public function addTrackedTime(ModuleTimeTrackData $trackedTime): self
    {
        if (!$this->trackedTime->contains($trackedTime)) {
            $this->trackedTime[] = $trackedTime;
            $trackedTime->setWorkSpaceUser($this);
        }

        return $this;
    }

    public function removeTrackedTime(ModuleTimeTrackData $trackedTime): self
    {
        if ($this->trackedTime->contains($trackedTime)) {
            $this->trackedTime->removeElement($trackedTime);
            // set the owning side to null (unless already changed)
            if ($trackedTime->getWorkSpaceUser() === $this) {
                $trackedTime->setWorkSpaceUser(null);
            }
        }

        return $this;
    }
}
