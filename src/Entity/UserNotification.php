<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"user"})
 * @ApiFilter(OrderFilter::class, properties={"id"}, arguments={"orderParameterName"="order"}))
 * @ORM\Entity(repositoryClass="App\Repository\UserNotificationRepository")
 */
class UserNotification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userNotifications")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $body;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $inviteHandled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $handled = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\WorkSpaceInvite", inversedBy="userNotification", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="invite_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $invite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getHandled(): ?bool
    {
        return $this->handled;
    }

    public function setHandled(bool $handled): self
    {
        $this->handled = $handled;

        return $this;
    }

    public function getInviteHandled(): ?bool
    {
        return $this->inviteHandled;
    }

    public function setInviteHandled(?bool $inviteHandled): self
    {
        $this->inviteHandled = $inviteHandled;

        return $this;
    }

    public function getInvite(): ?WorkSpaceInvite
    {
        return $this->invite;
    }

    public function setInvite(?WorkSpaceInvite $invite): self
    {
        $this->invite = $invite;

        return $this;
    }
}
