<?php

namespace App\Entity;

use Doctrine\Common\Collections\Criteria;
use JsonSerializable;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity()
 */
class Module implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpace", inversedBy="modules")
     */
    private $workSpace;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Channel", inversedBy="modules")
     */
    private $channel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleChatData", mappedBy="module", cascade={"persist","remove"})
     */
    private $moduleChatData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleTimeTrackData", mappedBy="module", cascade={"persist", "remove"})
     */
    private $moduleTimeTrackData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModuleKanBanLane", mappedBy="module", cascade={"persist", "remove"})
     */
    private $moduleKanBanLane;

    public function __construct()
    {
        $this->moduleChatData = new ArrayCollection();
        $this->moduleTimeTrackData = new ArrayCollection();
        $this->moduleKanBanLane = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    public function setChannel(?Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getModuleData()
    {
        switch($this->getType())
        {
            case "Chat":
                return $this->getModuleChatData()->toArray();

            case "TimeTracker":
                return $this->getTimeTrackDataOrderedById();

            case "Kanban":
                return ["lanes" => $this->getModuleKanBanLane()->toArray()];

            default:
                return "data";
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getModuleChatData(): Collection
    {
        return $this->moduleChatData;
    }

    public function addModuleChatData(ModuleChatData $moduleChatData): self
    {
        if (!$this->moduleChatData->contains($moduleChatData)) {
            $this->moduleChatData[] = $moduleChatData;
            $moduleChatData->setModule($this);
        }

        return $this;
    }

    public function removeModuleChatData(ModuleChatData $moduleChatData): self
    {
        if ($this->moduleChatData->contains($moduleChatData)) {
            $this->moduleChatData->removeElement($moduleChatData);
            // set the owning side to null (unless already changed)
            if ($moduleChatData->getModule() === $this) {
                $moduleChatData->setModule(null);
            }
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'channel'   => !empty($this->getChannel()) ? $this->getChannel()->getId() : null,
            'type'      => $this->getType()
        );
    }

    public function getTimeTrackDataOrderedById()
    {
        $criteria = Criteria::create()->orderBy(['id' => 'DESC']);
        return $this->getModuleTimeTrackData()->matching($criteria)->toArray();
    }

    public function getModuleTimeTrackData(): Collection
    {
        return $this->moduleTimeTrackData;
    }

    public function addModuleTimeTrackData(ModuleTimeTrackData $moduleTimeTrackData): self
    {
        if (!$this->moduleTimeTrackData->contains($moduleTimeTrackData)) {
            $this->moduleTimeTrackData[] = $moduleTimeTrackData;
            $moduleTimeTrackData->setModule($this);
        }

        return $this;
    }

    public function removeModuleTimeTrackData(ModuleTimeTrackData $moduleTimeTrackData): self
    {
        if ($this->moduleTimeTrackData->contains($moduleTimeTrackData)) {
            $this->moduleTimeTrackData->removeElement($moduleTimeTrackData);
            // set the owning side to null (unless already changed)
            if ($moduleTimeTrackData->getModule() === $this) {
                $moduleTimeTrackData->setModule(null);
            }
        }

        return $this;
    }

    public function getModuleKanBanLane(): Collection
    {
        return $this->moduleKanBanLane;
    }

    public function addModuleKanBanLane(ModuleKanBanLane $moduleKanBanLane): self
    {
        if (!$this->moduleKanBanLane->contains($moduleKanBanLane)) {
            $this->moduleKanBanLane[] = $moduleKanBanLane;
            $moduleKanBanLane->setModule($this);
        }

        return $this;
    }

    public function removeModuleKanBanLane(ModuleKanBanLane $moduleKanBanLane): self
    {
        if ($this->moduleKanBanLane->contains($moduleKanBanLane)) {
            $this->moduleKanBanLane->removeElement($moduleKanBanLane);
            // set the owning side to null (unless already changed)
            if ($moduleKanBanLane->getModule() === $this) {
                $moduleKanBanLane->setModule(null);
            }
        }

        return $this;
    }
}
