<?php

namespace App\Entity;

use JsonSerializable;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ModuleKanBanDataRepository")
 */
class ModuleKanBanData implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ModuleKanBanLane", inversedBy="data")
     * @ORM\JoinColumn(nullable=false)
     */
    private $moduleKanBanLane;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="moduleKanBanData")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $rowOrder;

    public function jsonSerialize()
    {
        return array(
            'id'        => (string) $this->getId(),
            'title'     => $this->getTitle(),
            'body'      => $this->getBody(),
            'user'      => !empty($this->getUser()) ? $this->getUser()->getFirstName() . $this->getUser()->getLastName() : null,
            'laneId'    => (string) $this->getModuleKanBanLane()->getId()
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getModuleKanBanLane(): ?ModuleKanBanLane
    {
        return $this->moduleKanBanLane;
    }

    public function setModuleKanBanLane(?ModuleKanBanLane $moduleKanBanLane): self
    {
        $this->moduleKanBanLane = $moduleKanBanLane;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRowOrder(): ?int
    {
        return $this->rowOrder;
    }

    public function setRowOrder(int $rowOrder): self
    {
        $this->rowOrder = $rowOrder;

        return $this;
    }
}
