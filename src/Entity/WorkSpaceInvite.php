<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity()
 */
class WorkSpaceInvite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="workSpaceInvites")
     */
    private $target;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpace", inversedBy="workSpaceInvites")
     */
    private $workSpace;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="workSpaceInvites")
     */
    private $sentBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accepted;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserNotification", mappedBy="invite")
     */
    private $userNotification;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTarget(): ?User
    {
        return $this->target;
    }

    public function setTarget(?User $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getSentBy(): ?User
    {
        return $this->sentBy;
    }

    public function setSentBy(?User $sentBy): self
    {
        $this->sentBy = $sentBy;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getUserNotification(): ?UserNotification
    {
        return $this->userNotification;
    }

    public function setUserNotification(?UserNotification $userNotification): self
    {
        $this->userNotification = $userNotification;

        // set (or unset) the owning side of the relation if necessary
        $newInvite = $userNotification === null ? null : $this;
        if ($newInvite !== $userNotification->getInvite()) {
            $userNotification->setInvite($newInvite);
        }

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }
}
