<?php

namespace App\Entity;

use JsonSerializable;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ModuleTimeTrackDataRepository")
 */
class ModuleTimeTrackData implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module", inversedBy="moduleTimeTrackData")
     */
    private $module;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="moduleTimeTrackData")
     */
    private $user;

    /**
     * @ORM\Column(type="bigint")
     */
    private $time;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkSpaceUser", inversedBy="trackedTime")
     * @ORM\JoinColumn(nullable=false)
     */
    private $workSpaceUser;

    public function jsonSerialize()
    {
        return array(
            'user'          => [
                'firstName' => $this->getUser()->getFirstName(),
                'lastName'  => $this->getUser()->getLastName()
            ],
            'time'          => $this->getTime(),
            'date'          => $this->getCreatedAt(),
            'title'         => $this->getTitle()
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getWorkSpaceUser(): ?WorkSpaceUser
    {
        return $this->workSpaceUser;
    }

    public function setWorkSpaceUser(?WorkSpaceUser $workSpaceUser): self
    {
        $this->workSpaceUser = $workSpaceUser;

        return $this;
    }
}
