<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class WorkSpaceUserRepository extends EntityRepository
{
    /**
     * @param $workspace
     * @param $roleOrder
     * @return array
     */
    public function getWorkSpaceUsers($workspace, $roleOrder)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT u.id from App:User u
                      LEFT JOIN u.workSpaceUsers wu
                      LEFT JOIN wu.workSpaceUserRoles wur
                      LEFT JOIN wur.workSpaceRole wr
                      WHERE wr.roleOrder >= :roleOrder
                      AND wu.workSpace = :workSpace'
            )
            ->setParameter('workSpace', $workspace)
            ->setParameter('roleOrder', $roleOrder)
            ->getArrayResult();
    }
}