<?php

namespace App\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;

class WorkSpaceRepository extends EntityRepository
{
    /**
     * @param $workspace
     * @param $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function userInWorkSpace($workspace, $user)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u
                     LEFT JOIN u.workSpaceUsers wu
                     LEFT JOIN wu.workSpace w
                     WHERE w = :workspace
                     AND u = :user'
            )
            ->setParameter('workspace', $workspace)
            ->setParameter('user', $user)
            ->getOneOrNullResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getMonthlyHours($workspace)
    {
        $firstDay = new DateTime('first day of this month');
        $lastDay = new DateTime('last day of this month');

        return $this->getEntityManager()
            ->createQuery(
                'SELECT SUM(td.time)
                      FROM App:WorkSpace w
                      LEFT JOIN w.modules m
                      LEFT JOIN m.moduleTimeTrackData td
                      WHERE td.id is NOT NULL
                      AND w = :workspace
                      AND td.createdAt >= :startDate
                      AND td.createdAt <= :endDate'
            )
            ->setParameter('workspace', $workspace)
            ->setParameter('startDate', $firstDay->format('Y-m-d 00:00:00'))
            ->setParameter('endDate', $lastDay->format('Y-m-d 23:59:59'))
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getPrevMonthlyHours($workspace)
    {
        $firstDay = new DateTime('first day of last month');
        $lastDay = new DateTime('last day of last month');

        return $this->getEntityManager()
            ->createQuery(
                'SELECT SUM(td.time)
                      FROM App:WorkSpace w
                      LEFT JOIN w.modules m
                      LEFT JOIN m.moduleTimeTrackData td
                      WHERE td.id is NOT NULL
                      AND w = :workspace
                      AND td.createdAt >= :startDate
                      AND td.createdAt <= :endDate'
            )
            ->setParameter('workspace', $workspace)
            ->setParameter('startDate', $firstDay->format('Y-m-d 00:00:00'))
            ->setParameter('endDate', $lastDay->format('Y-m-d 23:59:59'))
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getYearlyHours($workspace)
    {
        $year = new DateTime('this year');

        return $this->getEntityManager()
            ->createQuery(
                'SELECT SUM(td.time)
                      FROM App:WorkSpace w
                      LEFT JOIN w.modules m
                      LEFT JOIN m.moduleTimeTrackData td
                      WHERE td.id is NOT NULL
                      AND w = :workspace
                      AND td.createdAt >= :startDate
                      AND td.createdAt <= :endDate'
            )
            ->setParameter('workspace', $workspace)
            ->setParameter('startDate', $year->format('Y-01-01 00:00:00'))
            ->setParameter('endDate', $year->format('Y-12-31 23:59:59'))
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getPrevYearlyHours($workspace)
    {
        $year = new DateTime('previous year');

        return $this->getEntityManager()
            ->createQuery(
                'SELECT SUM(td.time)
                      FROM App:WorkSpace w
                      LEFT JOIN w.modules m
                      LEFT JOIN m.moduleTimeTrackData td
                      WHERE td.id is NOT NULL
                      AND w = :workspace
                      AND td.createdAt >= :startDate
                      AND td.createdAt <= :endDate'
            )
            ->setParameter('workspace', $workspace)
            ->setParameter('startDate', $year->format('Y-01-01 00:00:00'))
            ->setParameter('endDate', $year->format('Y-12-31 23:59:59'))
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getUserCount($workspace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT COUNT(u)
                      FROM App:WorkSpaceUser u
                      LEFT JOIN u.workSpace w
                      WHERE w = :workspace'
            )
            ->setParameter('workspace', $workspace)
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getMessageCount($workspace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT COUNT(d)
                      FROM App:WorkSpace w
                      LEFT JOIN w.modules m
                      LEFT JOIN m.moduleChatData d
                      WHERE d.id is NOT NULL
                      AND w = :workspace'
            )
            ->setParameter('workspace', $workspace)
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getGroupCount($workspace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT COUNT(g)
                      FROM App:ChannelGroup g
                      LEFT JOIN g.workSpace w
                      WHERE w = :workspace'
            )
            ->setParameter('workspace', $workspace)
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getChannelCount($workspace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT COUNT(c)
                      FROM App:Channel c
                      LEFT JOIN c.workSpace w
                      WHERE w = :workspace'
            )
            ->setParameter('workspace', $workspace)
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getModuleCount($workspace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT COUNT(m)
                      FROM App:Module m
                      LEFT JOIN m.workSpace w
                      WHERE w = :workspace'
            )
            ->setParameter('workspace', $workspace)
            ->getSingleScalarResult();
    }
}