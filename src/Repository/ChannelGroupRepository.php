<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ChannelGroupRepository extends EntityRepository
{
    /**
     * @param $channelGroup
     * @param $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function userInChannelGroup($channelGroup, $user)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u
                      LEFT JOIN u.channelGroupUsers cu
                      LEFT JOIN cu.channelGroup cg
                      WHERE cg = :channelGroup
                      AND u = :user'
            )
            ->setParameter('channelGroup', $channelGroup)
            ->setParameter('user', $user)
            ->getOneOrNullResult();
    }

    public function getChannelGroupUsers($channelGroup, $workSpace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT DISTINCT u.id
                      FROM App:ChannelGroupUser cu
                      LEFT JOIN cu.channelGroup cg
                      LEFT JOIN cg.workSpace w
                      LEFT JOIN cu.user u
                      WHERE cg = :channelGroup
                      AND w = :workSpace'
            )
            ->setParameter('channelGroup', $channelGroup)
            ->setParameter('workSpace', $workSpace)
            ->getArrayResult();
    }
}
