<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ChannelRepository extends EntityRepository
{
    /**
     * @param $channel
     * @param $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function userInChannel($channel, $user)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u
                     LEFT JOIN u.channelUsers cu
                     LEFT JOIN cu.channel c
                     WHERE c = :channel
                     AND u = :user'
            )
            ->setParameter('channel', $channel)
            ->setParameter('user', $user)
            ->getOneOrNullResult();
    }

    public function getChannelUsers($channel, $workSpace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT DISTINCT u.id
                      FROM App:ChannelUser cu
                      LEFT JOIN cu.channel c
                      LEFT JOIN c.workSpace w
                      LEFT JOIN cu.user u
                      WHERE c = :channel
                      AND w = :workSpace'
            )
            ->setParameter('channel', $channel)
            ->setParameter('workSpace', $workSpace)
            ->getArrayResult();
    }
}
