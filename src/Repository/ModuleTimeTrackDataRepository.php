<?php

namespace App\Repository;

use App\Entity\ModuleTimeTrackData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ModuleTimeTrackData|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModuleTimeTrackData|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModuleTimeTrackData[]    findAll()
 * @method ModuleTimeTrackData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleTimeTrackDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ModuleTimeTrackData::class);
    }

    // /**
    //  * @return ModuleTimeTrackData[] Returns an array of ModuleTimeTrackData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModuleTimeTrackData
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
