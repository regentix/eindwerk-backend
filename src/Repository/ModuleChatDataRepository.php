<?php

namespace App\Repository;

use App\Entity\ModuleChatData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ModuleChatData|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModuleChatData|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModuleChatData[]    findAll()
 * @method ModuleChatData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleChatDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ModuleChatData::class);
    }

    // /**
    //  * @return ModuleChatData[] Returns an array of ModuleChatData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModuleChatData
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
