<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ModuleKanBanLaneRepository extends EntityRepository
{
    public function getLaneDataByOrder($lane)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT d 
                      FROM App:ModuleKanBanData d
                      WHERE d.moduleKanBanLane = :lane
                      ORDER BY d.rowOrder'
            )
            ->setParameter('lane', $lane)
            ->getResult();
    }
}