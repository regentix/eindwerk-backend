<?php

namespace App\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getUserWorkSpaceData($workSpace)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT w, wm, wmc, c, ccg, cg, cu, u, cgu, gu, cbr, cgbr
                 FROM App:WorkSpace w
                 LEFT JOIN w.modules wm
                 LEFT JOIN wm.channel wmc
                 LEFT JOIN w.channel c
                 LEFT JOIN w.channelGroup cg
                 LEFT JOIN c.channelGroup ccg
                 LEFT JOIN c.channelUser cu
                 LEFT JOIN cu.user u
                 LEFT JOIN c.baseRole cbr
                 LEFT JOIN cg.channelGroupUsers cgu
                 LEFT JOIN cgu.user gu
                 LEFT JOIN cg.baseRole cgbr
                 WHERE w = :workSpace
                 ORDER BY wm.type ASC, cg.name ASC, c.name ASC'
            )
            ->setParameter('workSpace', $workSpace)
            ->getArrayResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getMonthlyHours($workspace, $user)
    {
        $firstDay = new DateTime('first day of this month');
        $lastDay = new DateTime('last day of this month');

        return $this->getEntityManager()
            ->createQuery(
                'SELECT SUM(td.time)
                 FROM App:WorkSpace w
                 LEFT JOIN w.modules m
                 LEFT JOIN m.moduleTimeTrackData td
                 WHERE td.id is NOT NULL
                 AND w = :workspace
                 AND td.user = :user
                 AND td.createdAt >= :startDate
                 AND td.createdAt <= :endDate'
            )
            ->setParameter('user', $user)
            ->setParameter('workspace', $workspace)
            ->setParameter('startDate', $firstDay->format('Y-m-d 00:00:00'))
            ->setParameter('endDate', $lastDay->format('Y-m-d 23:59:59'))
            ->getSingleScalarResult();
    }

    /**
     * @param $workspace
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException|\Exception
     */
    public function getPrevMonthlyHours($workspace, $user)
    {
        $firstDay = new DateTime('first day of last month');
        $lastDay = new DateTime('last day of last month');

        return $this->getEntityManager()
            ->createQuery(
                'SELECT SUM(td.time)
                      FROM App:WorkSpace w
                      LEFT JOIN w.modules m
                      LEFT JOIN m.moduleTimeTrackData td
                      WHERE td.id is NOT NULL
                      AND w = :workspace
                      AND td.user = :user
                      AND td.createdAt >= :startDate
                      AND td.createdAt <= :endDate'
            )
            ->setParameter('user', $user)
            ->setParameter('workspace', $workspace)
            ->setParameter('startDate', $firstDay->format('Y-m-d 00:00:00'))
            ->setParameter('endDate', $lastDay->format('Y-m-d 23:59:59'))
            ->getSingleScalarResult();
    }

    public function deleteChannelUsers($workSpace, $user)
    {
        $channelUser = $this->getEntityManager()
            ->createQuery(
                'SELECT cu FROM App:ChannelUser cu
                           LEFT JOIN cu.user u
                           LEFT JOIN cu.channel c
                           LEFT JOIN c.workSpace w
                           WHERE u = :user
                           AND w = :workSpace'
            )
            ->setParameter('user', $user)
            ->setParameter('workSpace', $workSpace)
            ->execute();

        $this->getEntityManager()
            ->createQuery(
                'DELETE App:ChannelUser cu
                 WHERE cu = :channelUser'
            )
            ->setParameter('channelUser', $channelUser)
            ->execute();
    }

    public function deleteChannelGroupUsers($workSpace, $user)
    {
        $channelGroupUser = $this->getEntityManager()
            ->createQuery(
                'SELECT cgu FROM App:ChannelGroupUser cgu
                           LEFT JOIN cgu.user u
                           LEFT JOIN cgu.channelGroup cg
                           LEFT JOIN cg.workSpace w
                           WHERE u = :user
                           AND w = :workSpace'
            )
            ->setParameter('user', $user)
            ->setParameter('workSpace', $workSpace)
            ->execute();

        $this->getEntityManager()
            ->createQuery(
                'DELETE App:ChannelGroupUser cgu
                 WHERE cgu = :channelGroupUser'
            )
            ->setParameter('channelGroupUser', $channelGroupUser)
            ->execute();
    }

    public function getLastWorkSpaceInvite($workSpace, $user)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT i FROM App:WorkSpaceInvite i
                      LEFT JOIN i.target t
                      LEFT JOIN i.workSpace w
                      WHERE w = :workSpace
                      AND t = :user
                      ORDER BY i.id DESC'
            )
            ->setParameter('user', $user)
            ->setParameter('workSpace', $workSpace)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
