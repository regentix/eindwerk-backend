<?php

namespace App\Service;

use Pusher\Pusher;

class PusherService
{
    protected $pusher;

    /**
     * PusherService constructor.
     * @throws \Pusher\PusherException
     */
    public function __construct()
    {
        $options = [
            'cluster'   => getenv('PUSHER_CLUSTER'),
        ];

        $this->pusher = new Pusher (
            getenv('PUSHER_KEY'),
            getenv('PUSHER_SECRET'),
            getenv('PUSHER_APP_ID'),
            $options
        );
    }

    /**
     * @param $channel
     * @param $event
     * @param $data
     * @throws \Pusher\PusherException
     */
    public function notification($channel, $event, $data)
    {
        $this->pusher->trigger($channel, $event, $data);
    }
}