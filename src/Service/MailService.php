<?php

namespace App\Service;

use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;

class MailService
{
    protected $mailer;
    protected $templating;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating)
    {
        $this->mailer     = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param $subject
     * @param $from
     * @param $to
     * @param $token
     */
    public function sendResetMail($subject, $from, $to, $token) {
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->templating->render(
                    'mail/mail.html.twig',
                    ['token' => $token]
                ), 'text/html'
            )
            ->setCharset('utf-8');

        $this->mailer->send($message);
    }
}