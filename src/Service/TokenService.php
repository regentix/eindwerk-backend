<?php

namespace App\Service;

use App\Entity\User;
use Carbon\Carbon;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Response;

class TokenService
{
    protected $attachRefreshTokenOnSuccessListener;
    protected $refreshTokenManager;

    public function __construct($attachRefreshTokenOnSuccessListener, $refreshTokenManager)
    {
        $this->attachRefreshTokenOnSuccessListener  = $attachRefreshTokenOnSuccessListener;
        $this->refreshTokenManager                  = $refreshTokenManager;
    }

    /**
     * @param $length
     * @return string
     * @throws \Exception
     */
    public function generateResetToken($length)
    {
        return bin2hex(random_bytes($length));
    }

    /**
     * @param User $user
     * @return string
     */
    public function getRefreshTokenForUser(User $user)
    {
        $response        = new Response();
        $jwtSuccessEvent = new AuthenticationSuccessEvent(array(), $user, $response);

        $this->attachRefreshTokenOnSuccessListener->attachRefreshToken($jwtSuccessEvent);

        $refreshToken = $this->refreshTokenManager->getLastFromUsername($user->getUsername());

        return $refreshToken->getRefreshToken();
    }

    /**
     * @param $userToken
     * @param $token
     * @return boolean
     */
    public function validateToken($userToken, $token)
    {
        $expiresAt = Carbon::instance($userToken->getExpiresAt());

        $isExpired = $expiresAt->isPast();
        $isValid   = $userToken->getToken() == $token;

        return !$isExpired && $isValid;
    }
}