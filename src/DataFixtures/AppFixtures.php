<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\WorkSpace;
use App\Entity\User;
use App\Entity\WorkSpaceUser;
use App\Entity\WorkSpaceRole;
use App\Entity\WorkSpaceUserRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Psr\Log\LoggerInterface;

class AppFixtures extends Fixture
{
    protected $faker;
    protected $encoder;
    protected $logger;

    public function __construct(UserPasswordEncoderInterface $encoder, LoggerInterface $logger)
    {
        $this->encoder = $encoder;
        $this->logger = $logger;
    }

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();
        $roles = [
            [
                "role" => "USER",
                "roleOrder" => 1
            ],
            [
                "role" => "ADMIN",
                "roleOrder" => 98
            ],
            [
                "role" => "OWNER",
                "roleOrder" => 99
            ]
        ];

        $this->createData($manager, $roles, "Yves", "Sterckx", "yves.sterckx@hotmail.com");
        $this->createData($manager, $roles, "Daan", "Boschmans", "daan.boschmans@outlook.com");
    }

    public function createData($manager, $roles, $firstName, $lastName, $email)
    {
        $this->logger->info("creating user for $email");

        $owner = new User();
        $owner->setFirstName($firstName);
        $owner->setLastName($lastName);
        $owner->setEmail($email);
        $owner->setPassword($this->encoder->encodePassword($owner, "123123"));

        $manager->persist($owner);

        for ($a = 1; $a <= 3; $a++) {
            $this->logger->info("creating workspace $a");

            $workSpace = new WorkSpace();
            $workSpace->setName($this->faker->domainWord);
            $workSpace->setOwner($owner);

            $manager->persist($workSpace);

            $manager->flush();

            for ($c = 1; $c <= 6; $c++) {
                $this->logger->info("creating workspace user $c for workspace " . $workSpace->getName());

                $user = new User();
                $user->setFirstName($this->faker->firstName());
                $user->setLastName($this->faker->lastName());
                $user->setEmail($this->faker->email());
                $user->setPassword($this->encoder->encodePassword($user, "123123"));

                $manager->persist($user);

                $workSpaceUser = new WorkSpaceUser();
                $workSpaceUser->setWorkSpace($workSpace);
                $workSpaceUser->setUser($user);

                $manager->persist($workSpaceUser);

                $userRole = $manager->getRepository(WorkSpaceRole::class)->findOneBy(["workSpace" => $workSpace, "role" => "USER"]);

                $workSpaceUserRole = new WorkSpaceUserRole();
                $workSpaceUserRole->setWorkSpaceRole($userRole);
                $workSpaceUserRole->setWorkSpaceUser($workSpaceUser);

                $manager->persist($workSpaceUserRole);

                $manager->flush();
            }
        }
    }
}
