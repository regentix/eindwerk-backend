<?php

namespace App\Security\Voter;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\WorkSpace;

class WorkSpaceVoter extends Voter
{
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository(WorkSpace::class);
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['WORKSPACE_EDIT', 'WORKSPACE_VIEW', 'WORKSPACE_DELETE']) && $subject instanceof WorkSpace;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        $ownerId = $subject->getOwner()->getId();
        $userId  = $user->getId();

        switch ($attribute) {
            case 'WORKSPACE_EDIT':
                if ($ownerId == $userId) return true;
                break;
            case 'WORKSPACE_VIEW':
                if ($ownerId == $userId)                                         return true;
                if (!empty($this->repository->userInWorkSpace($subject, $user))) return true;
                break;
            case 'WORKSPACE_DELETE':
                if ($ownerId == $userId)                                         return true;
                break;
        }

        return false;
    }
}
