<?php

namespace App\Controller;

use App\Entity\ConfirmationToken;
use App\Entity\ResetToken;
use App\Entity\User;
use App\Entity\WorkSpace;
use App\Entity\WorkSpaceRole;
use App\Entity\WorkSpaceUserRole;
use App\Service\MailService;
use App\Service\PusherService;
use App\Service\TokenService;
use App\Service\UserService;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\Entity\WorkSpaceUser;

class AuthController extends AbstractController
{
    protected $mailService;
    protected $tokenService;
    protected $jwtManager;
    protected $tokenStorageInterface;
    protected $userService;
    protected $pusherService;

    public function __construct
    (
        MailService $mailService,
        TokenService $tokenService,
        UserService $userService,
        PusherService $pusherService
    )
    {
        $this->mailService  = $mailService;
        $this->tokenService = $tokenService;
        $this->userService  = $userService;
        $this->pusherService = $pusherService;
    }

    /**
     * @Route("/api/user/register", name="register", methods={"POST"})
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, JWTTokenManagerInterface $JWTManager)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        if ($em->getRepository(User::class)->findBy(['email' => $requestData->email])) {
            return new JsonResponse(['message' => 'The provided email is already taken.'], 409);
        }

        $user = new User();

        $user->setEmail($requestData->email);
        $user->setFirstName($requestData->firstName);
        $user->setLastName($requestData->lastName);
        $user->setTag('@'.strtolower($requestData->firstName.$requestData->lastName));
        $user->setPassword($encoder->encodePassword($user, $requestData->password));

        $em->persist($user);
        $em->flush();

        $userData = [
            'workSpaceUser'  => [],
            'firstName'      => $user->getFirstName(),
            'lastName'       => $user->getLastName(),
            'status'         => $user->getStatus(),
            'email'          => $user->getEmail(),
            'id'             => $user->getId()
        ];

        $responseData = [
            'token'        => $JWTManager->create($user),
            'user'         => $userData,
            'refreshToken' => $this->tokenService->getRefreshTokenForUser($user)
        ];

        return new JsonResponse($responseData, 200);
    }

    /**
     * @Route("/api/user/request-reset-password", name="request_reset_password", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function requestResetPassword(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $em->getRepository(User::class)->findOneBy(['email' => $requestData->email]);

        if (!$user instanceof User) {
            return new JsonResponse(['message' => 'No user found for the provided email.'], 404);
        }

        $token = $this->tokenService->generateResetToken(12);

        try {
            $this->mailService->sendResetMail('Reset token', $_ENV['MAILER_ADDRESS'], $requestData->email, $token);
        } catch(\Exception $exception) {
            return new JsonResponse([
                'message'   => 'Something went wrong while sending the email.',
                'error'     => $exception->getMessage()
            ], 500);
        }

        $expiresAt = new Carbon();
        $expiresAt->addMinutes(5);

        if (!$user->getResetToken()) {
            $resetToken = new ResetToken();
            $resetToken->setToken($token);
            $resetToken->setExpiresAt($expiresAt);

            $user->setResetToken($resetToken);

            $em->persist($resetToken);

        } else {
            $user->getResetToken()->setToken($token);
            $user->getResetToken()->setExpiresAt($expiresAt);
        }

        $em->flush();

        return new JsonResponse(['message' => 'Email successfully sent.'], 200);
    }

    /**
     * @Route("/api/user/reset-password", name="reset_password", methods={"POST"})
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $em->getRepository(User::class)->findOneBy(['email' => $requestData->email]);

        if (!$user instanceof User) {
            return new JsonResponse(['message' => 'No user found for the provided email.'], 404);
        }

        $resetToken = $user->getResetToken();

        if (!$resetToken instanceof ResetToken) {
            return new JsonResponse(['message' => 'The user didn\'t request to change password.'], 403);
        }

        $confirmationToken = $resetToken->getConfirmationToken();

        if (!$confirmationToken instanceof ConfirmationToken) {
            return new JsonResponse(['message' => 'No confirmation token found. Please make sure you\'ve verified the reset token.'], 403);
        }

        $isValid = $this->tokenService->validateToken($confirmationToken, $requestData->confirmationToken);

        if (!$isValid) {
            return new JsonResponse(['message' => 'The confirmation token is invalid.'], 403);
        }

        if ($requestData->password !== $requestData->confirmPassword) {
            return new JsonResponse(['message' => 'Password confirmation doesn\'t match.'], 403);
        }

        $user->setPassword($encoder->encodePassword($user, $requestData->password));

        $em->remove($user->getResetToken());

        $em->flush();

        return new JsonResponse(['message' => 'Password has been reset.'], 200);
    }

    /**
     * @Route("/api/user/get", name="get_user", methods={"POST"})
     *
     * @return array|false|mixed
     */
    public function getUser()
    {
        $user = $this->userService->getCurrentUser();

        $workSpaceUsers = [];

        foreach ($user->getWorkSpaceUsers() as $workSpaceUser) {
            $workSpaceUsers[] = [
                "workSpace" => $workSpaceUser->getWorkSpace()->getId(),
                "roleOrder" => $workSpaceUser->getWorkSpaceUserRole()->getWorkSpaceRole()->getRoleOrder()
            ];
        }

        $data = [
            'firstName'     => $user->getFirstName(),
            'lastName'      => $user->getLastName(),
            'email'         => $user->getEmail(),
            'status'        => $user->getStatus(),
            'id'            => $user->getId(),
            'workSpaceUser' => $workSpaceUsers
        ];

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/user/set/status", name="set_user_status", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setUserStatus(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $this->userService->getCurrentUser();

        if (!$user instanceof User) {
            return new JsonResponse(['message' => 'No user found for the provided email.'], 404);
        }

        $user->setStatus($requestData->status);

        $em->flush();

        return new JsonResponse('User status has been updated.', 200);
    }

    /**
     * @Route("/api/user/workspace/role", name="user_set_workspace_role", methods={"POST"})
     */
    public function updateWorkSpaceRole(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $em->getRepository(User::class)->find($requestData->user);

        if (empty($user)) {
            return new JsonResponse(['message' => 'The provided user does not exist.'], 404);
        }

        $workSpaceRole = $em->getRepository(WorkSpaceRole::class)->find($requestData->currentRole);

        if (empty($workSpaceRole)) {
            return new JsonResponse(['message' => 'The provided workspace role does not exist.'], 404);
        }

        $targetRole = $em->getRepository(WorkSpaceRole::class)->find($requestData->targetRole);

        if (empty($targetRole)) {
            return new JsonResponse(['message' => 'The provided target role does not exist.'], 404);
        }

        $workSpaceUser = $em->getRepository(WorkSpaceUser::class)->findOneBy(['user' => $user, 'workSpace' => $workSpaceRole->getWorkSpace()]);

        if (empty($workSpaceUser)) {
            return new JsonResponse(['message' => 'The provided user does not exist.'], 404);
        }

        $workSpaceUserRole = $em->getRepository(WorkSpaceUserRole::class)->findOneBy(['workSpaceRole' => $workSpaceRole, 'workSpaceUser' => $workSpaceUser]);

        if (empty($workSpaceUserRole)) {
            return new JsonResponse(['message' => 'The provided workspace user role does not exist.'], 404);
        }

        $workSpaceUserRole->setWorkSpaceRole($targetRole);
        $workSpaceUserRole->setWorkSpaceUser($workSpaceUser);

        $em->flush();

        foreach ($user->getChannelUsers() as $channelUser) {
            if ($channelUser->getChannel()->getBaseRole()->getRoleOrder() > $targetRole->getRoleOrder()) {
                $em->remove($channelUser);
                $em->flush();
            }
        }

        foreach ($user->getChannelGroupUsers() as $channelGroupUser) {
            if ($channelGroupUser->getChannelGroup()->getBaseRole()->getRoleOrder() > $targetRole->getRoleOrder()) {
                $em->remove($channelGroupUser);
                $em->flush();
            }
        }

        $pusherResponseData = [
            'firstName' => $workSpaceUser->getUser()->getFirstName(),
            'lastName'  => $workSpaceUser->getUser()->getLastName(),
            'id'        => $workSpaceUser->getUser()->getId(),
            'roles'     => [
                [
                    '_id'   => $targetRole->getId(),
                    'role'  => $targetRole->getRole(),
                    'roleOrder'  => $targetRole->getRoleOrder()
                ]
            ]
        ];

        $channel = 'workspace-' . $workSpaceUser->getWorkSpace()->getId() . '-user-role-update';

        $this->pusherService->notification($channel, 'update', $pusherResponseData);

        return new JsonResponse(['message' => 'User role updated.'], 200);
    }
}
