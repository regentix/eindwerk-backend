<?php

namespace App\Controller;

use App\Service\UserService;
use DateTime;
use App\Entity\UserNotification;
use App\Entity\User;
use App\Entity\WorkSpace;
use App\Entity\WorkSpaceInvite;
use App\Entity\WorkSpaceUser;
use App\Entity\WorkSpaceLog;
use App\Service\PusherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\WorkSpaceUserRole;
use App\Entity\WorkSpaceRole;

class NotificationController extends AbstractController
{
    protected $pusherService;
    protected $jwtManager;
    protected $tokenStorageInterface;
    protected $userService;

    public function __construct(
        PusherService $pusherService,
        UserService $userService
    ) {
        $this->pusherService         = $pusherService;
        $this->userService           = $userService;
    }

    /**
     * @Route(path="/api/send/notification", name="notification", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function sendNotification(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $em->getRepository(User::class)->findOneBy(['email' => $requestData->email]);

        if (empty($user)) {
            return new JsonResponse(['message' => 'No user found for the provided email.'], 404);
        }

        if (empty($requestData->body)) {
            return new JsonResponse(['message' => 'Please provide a notification body.'], 403);
        }

        $userNotification = new UserNotification();

        $userNotification->setUser($user);
        $userNotification->setType('notification');
        $userNotification->setBody($requestData->body);

        $em->persist($userNotification);
        $em->flush();

        $pusherResponseData = [
            'body'          => $userNotification->getBody(),
            'inviteHandled' => null,
            'handled'       => $userNotification->getHandled(),
            'type'          => $userNotification->getType(),
            'invite'        => null,
        ];

        $this->pusherService->notification($requestData->email, 'notification', $pusherResponseData);

        return new JsonResponse(['message' => 'Notification has been sent.'], 200);
    }

    /**
     * @Route(path="/api/send/invite", name="invite", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function sendInvite(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->findOneBy(['id' => $requestData->workSpace]);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'Please provide an existing workspace.'], 404);
        }

        $sentBy    = $this->userService->getCurrentUser();

        $failedInvites = [];
        $successInvites = 0;

        foreach ($requestData->users as $user) {
            $target          = $em->getRepository(User::class)->findOneBy(['id' => $user]);
            $workSpaceUser   = $em->getRepository(WorkSpaceUser::class)->findOneBy(['user' => $target, "workSpace" => $workSpace]);

            if ($target == $sentBy) {
                continue;
            }


            if (!empty($workSpaceUser)) {
                $failedInvites[] = $target->getEmail();
                continue;
            }

            if (!empty($target)) {

                $workSpaceInvite = new WorkSpaceInvite();
                $workSpaceInvite->setTarget($target);
                $workSpaceInvite->setWorkSpace($workSpace);
                $workSpaceInvite->setSentBy($sentBy);
                $workSpaceInvite->setAccepted(false);

                $em->persist($workSpaceInvite);

                $userNotification = new UserNotification();
                $userNotification->setUser($target);
                $userNotification->setType('invite');
                $userNotification->setBody("You've been invited to join the " . $workSpace->getName() . " workspace.");
                $userNotification->setInvite($workSpaceInvite);

                $em->persist($userNotification);
                $em->flush();

                $successInvites++;

                $pusherResponseData = [
                    'body'          => $userNotification->getBody(),
                    'inviteHandled' => $userNotification->getInviteHandled(),
                    'handled'       => $userNotification->getHandled(),
                    'type'          => $userNotification->getType(),
                    'invite'        => [
                        'workSpace' => [
                            '_id'   => $userNotification->getInvite()->getWorkSpace()->getId()
                        ],
                        'accepted'  => $userNotification->getInvite()->getAccepted()
                    ],
                ];

                $this->pusherService->notification($target->getEmail(), 'invite', $pusherResponseData);
            }
        }

        $responseData = [
            'successCount'  => $successInvites,
            'failedInvites' => $failedInvites
        ];

        return new JsonResponse($responseData, 200);
    }

    /**
     * @Route(path="/api/handle/invite", name="handle_invite", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException|\Exception
     */
    public function handleInvite(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $target = $this->userService->getCurrentUser();

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $workSpaceUser = $em->getRepository(WorkSpaceUser::class)->findOneBy(['workSpace' => $requestData->workSpace, 'user' => $target]);

        if (!empty($workSpaceUser)) {
            return new JsonResponse(['message' => 'The user is already a part of this workspace.'], 403);
        }

        $workSpaceInvite = $em->getRepository(User::class)->getLastWorkSpaceInvite($workSpace, $target);

        if (empty($workSpaceInvite)) {
            return new JsonResponse(['message' => 'The user has not been invited to this workspace.'], 404);
        }

        if ($requestData->accepted) {
            $workSpaceInvite->setAccepted(true);
            $workSpaceInvite->setDeleted(true);
            $workSpaceInvite->getUserNotification()->setInviteHandled(true);

            $workSpaceLog = new WorkSpaceLog();
            $workSpaceLog->setBody($target->getFirstName() . " " . $target->getLastName() . " has accepted the workspace invitation.");
            $workSpaceLog->setDate(new DateTime('now'));
            $workSpaceLog->setWorkSpace($workSpaceInvite->getWorkSpace());
            $workSpaceLog->setType('success');

            $em->persist($workSpaceLog);

            $workSpaceUser = new WorkSpaceUser();
            $workSpaceUser->setWorkSpace($workSpaceInvite->getWorkSpace());
            $workSpaceUser->setUser($workSpaceInvite->getTarget());

            $em->persist($workSpaceUser);

            $userRole = $em->getRepository(WorkSpaceRole::class)->findOneBy(['workSpace' => $requestData->workSpace, 'role' => 'USER']);
            $workSpaceUserRole = new WorkSpaceUserRole();
            $workSpaceUserRole->setWorkSpaceUser($workSpaceUser);
            $workSpaceUserRole->setWorkSpaceRole($userRole);

            $em->persist($workSpaceUserRole);

            $userNotification = new UserNotification();
            $userNotification->setUser($workSpaceInvite->getSentBy());
            $userNotification->setType('notification');
            $userNotification->setBody($target->getFirstName() . " " . $target->getLastName() . " has accepted your invite to join the " . $workSpaceInvite->getWorkSpace()->getName() . " workspace.");

            $em->persist($userNotification);
            $em->flush();

            $pusherResponseData = [
                'body'          => $userNotification->getBody(),
                'inviteHandled' => null,
                'handled'       => $userNotification->getHandled(),
                'type'          => $userNotification->getType(),
                'invite'        => null,
                'accepted'      => true,
            ];

            $this->pusherService->notification($workSpaceInvite->getSentBy()->getEmail(), 'notification', $pusherResponseData);

            $workSpaceData = [
                'target' => 'userCount',
                'value'  => 1
            ];

            $workSpaceDataChannel = 'workspace-' . $workSpaceInvite->getWorkSpace()->getId() . '-data';

            $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

            $workSpaceUserArrayChannel = 'user-' . $workSpaceUser->getUser()->getId() . '-invite';

            $workSpaceArrayData = [
                'firstName' => $workSpaceUser->getUser()->getFirstName(),
                'lastName'  => $workSpaceUser->getUser()->getLastName(),
                'id'        => $workSpaceUser->getId(),
                'roles'     => [
                    'role'      => $workSpaceUserRole->getWorkSpaceRole()->getRole(),
                    'roleOrder' => $workSpaceUserRole->getWorkSpaceRole()->getRoleOrder(),
                    '_id'       => $workSpaceUserRole->getWorkSpaceRole()->getId()
                ]
            ];

            $this->pusherService->notification($workSpaceUserArrayChannel, 'update', $workSpaceArrayData);

            $this->pusherService->notification("workspace-" . $workSpaceInvite->getWorkSpace()->getId() . '-new-user', 'update', [
                'firstName' => $target->getFirstName(),
                'lastName'  => $target->getLastName(),
                'id'        => $target->getId(),
                'roles'     => [
                    [
                        'role'      => $userRole->getRole(),
                        'roleOrder' => $userRole->getRoleOrder(),
                        '_id'       => $userRole->getId()
                    ]
                ]
            ]);
            
            return new JsonResponse([
                'name'          => $workSpaceInvite->getWorkSpace()->getName(),
                '_id'           => $workSpaceInvite->getWorkSpace()->getId(),
                'id'            => '/api/work_spaces/' . $workSpaceInvite->getWorkSpace()->getId(),
                'owner'         => [
                    '_id'       => $workSpaceInvite->getWorkSpace()->getOwner()->getId()
                ],
                'image'         => [
                    'filePath'  => $workSpaceInvite->getWorkSpace()->getImage()->getFilePath()
                ],
                'workSpaceUser' => [
                    'workSpace' => $workSpaceUser->getWorkSpace()->getId(),
                    'roleOrder' => $workSpaceUserRole->getWorkSpaceRole()->getRoleOrder()
                ]
            ], 200);
        } else {
            $workSpaceInvite->setDeleted(true);
            $workSpaceInvite->getUserNotification()->setInviteHandled(true);

            $workSpaceLog = new WorkSpaceLog();
            $workSpaceLog->setBody($target->getFirstName() . " " . $target->getLastName() . " has declined the workspace invitation.");
            $workSpaceLog->setDate(new DateTime('now'));
            $workSpaceLog->setWorkSpace($workSpaceInvite->getWorkSpace());
            $workSpaceLog->setType('error');

            $em->persist($workSpaceLog);

            $userNotification = new UserNotification();
            $userNotification->setUser($workSpaceInvite->getSentBy());
            $userNotification->setType('notification');
            $userNotification->setBody($target->getFirstName() . " " . $target->getLastName() . " has declined your invite to join the " . $workSpaceInvite->getWorkSpace()->getName() . " workspace.");

            $em->persist($userNotification);
            $em->flush();

            $pusherResponseData = [
                'body'          => $userNotification->getBody(),
                'inviteHandled' => null,
                'handled'       => $userNotification->getHandled(),
                'type'          => $userNotification->getType(),
                'invite'        => null,
                'accepted'      => false,
            ];

            $this->pusherService->notification($workSpaceInvite->getSentBy()->getEmail(), 'notification', $pusherResponseData);
        }

        return new JsonResponse(['message' => 'User declined the workspace invite.'], 200);
    }

    /**
     * @Route(path="/api/handle/notifications", name="handle_notification", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function updateHandled()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->userService->getCurrentUser();

        $notifications = $em
            ->createQuery('SELECT n FROM App:UserNotification n LEFT JOIN n.user u WHERE n.handled = 0 AND u = :user')
            ->setParameter('user', $user)
            ->getResult();

        foreach ($notifications as $notification) {
            $notification->setHandled(true);
        }

        $em->flush();

        return new JsonResponse(['message' => 'Notifications successfully handled.'], 200);
    }
}