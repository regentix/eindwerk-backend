<?php

namespace App\Controller;

use App\Entity\ModuleKanBanData;
use App\Entity\ModuleKanBanLane;
use App\Entity\ModuleTimeTrackData;
use App\Entity\User;
use App\Entity\WorkSpaceLog;
use App\Entity\WorkSpaceUser;
use App\Service\PusherService;
use App\Service\UserService;
use DateTime;
use App\Entity\Channel;
use App\Entity\Module;
use App\Entity\ModuleChatData;
use App\Entity\WorkSpace;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ModuleController extends AbstractController
{
    protected $jwtManager;
    protected $tokenStorageInterface;
    protected $pusherService;
    protected $userService;

    public function __construct(
        PusherService $pusherService,
        UserService $userService
    )
    {
        $this->pusherService         = $pusherService;
        $this->userService           = $userService;
    }

    /**
     * @Route("/api/module/get/data", name="get_module_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $module = $em->getRepository(Module::class)->findOneBy(["workSpace" => $workSpace, "id" => $requestData->module]);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        return new JsonResponse($module->getModuleData(),200);
    }

    /**
     * @Route("/api/module/create", name="create_module", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createModule(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        $user = $this->userService->getCurrentUser();

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $responseData = [
            "id"        => "",
            "name"      => "",
            "type"      => "",
            "channel"   => (object) []
        ];

        if (empty($requestData->channel)) {
            $module = new Module();

            $module->setWorkSpace($workSpace);
            $module->setName($requestData->name);
            $module->setType($requestData->type);

            $em->persist($module);
            $em->flush();

            $logBody = $user->getFirstName() . " " . $user->getLastName() . " created a $requestData->type module";

            $responseData["id"]   = $module->getId();
            $responseData["name"] = $module->getName();
            $responseData["type"] = $module->getType();
        } else {

            $channel = $em->getRepository(Channel::class)->find($requestData->channel);

            if (empty($channel)) {
                return new JsonResponse(['message' => 'The provided channel does not exist.'], 404);
            }

            $module = new Module();

            $module->setWorkSpace($workSpace);
            $module->setChannel($channel);
            $module->setName($requestData->name);
            $module->setType($requestData->type);

            $em->persist($module);
            $em->flush();

            $logBody = $user->getFirstName() . " " .  $user->getLastName() . " created a $requestData->type module in " . $channel->getName();

            $responseData["id"]      = $module->getId();
            $responseData["name"]    = $module->getName();
            $responseData["type"]    = $module->getType();
            $responseData["channel"] = [
                "id"   => $channel->getId(),
                'name' => $channel->getName()
            ];
        }

        $workSpaceLog = new WorkSpaceLog();
        $workSpaceLog->setDate(new DateTime('now'));
        $workSpaceLog->setWorkSpace($workSpace);
        $workSpaceLog->setBody($logBody);
        $workSpaceLog->setType('info');

        $em->persist($workSpaceLog);
        $em->flush();

        $workSpaceData = [
            'target' => 'moduleCount',
            'value'  => 1
        ];

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        $moduleChannel = 'workspace-' . $workSpace->getId() . '-create-module';

        $this->pusherService->notification($moduleChannel, 'create', $responseData);

        return new JsonResponse("Module successfully created.", 200);
    }

    /**
     * @Route("/api/module/create/chat/data", name="create_module_chat_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createChatData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $this->userService->getCurrentUser();

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $pusherChannel = 'module-' . $module->getId();

        $chatData = new ModuleChatData();
        $chatData->setModule($module);
        $chatData->setBody($requestData->body);
        $chatData->setDate(new DateTime('now'));
        $chatData->setSentBy($user);

        $em->persist($chatData);
        $em->flush();

        $this->pusherService->notification($pusherChannel, 'update', $chatData);

        $workSpaceDataChannel = 'workspace-' . $module->getWorkSpace()->getId() . '-data';

        $workSpaceData = [
            'target' => 'messageCount',
            'value'  => 1
        ];

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        return new JsonResponse(['message' => 'Module data created.'], 200);
    }

    /**
     * @Route("/api/module/create/lane", name="create_module_kanban_lane", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createLane(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $pusherChannel = 'module-' . $module->getId();

        $lane = new ModuleKanBanLane();
        $lane->setModule($module);
        $lane->setTitle($requestData->title);
        $lane->setColOrder($requestData->colOrder);

        $em->persist($lane);
        $em->flush();

        $this->pusherService->notification($pusherChannel, 'update', $lane);

        return new JsonResponse(['message' => 'Lane created.'], 200);
    }

    /**
     * @Route("/api/module/create/lane/data", name="create_module_kanban_lane_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createLaneData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $lane = $em->getRepository(ModuleKanBanLane::class)->find($requestData->lane);

        if (empty($lane)) {
            return new JsonResponse(['message' => 'The provided lane does not exist.'], 404);
        }

        $user = $em->getRepository(User::class)->find($requestData->user);

        if (empty($user)) {
            return new JsonResponse(['message' => 'The provided user does not exist.'], 404);
        }

        $pusherChannel = 'module-' . $module->getId() . '-lane-data';

        $laneData = new ModuleKanBanData();
        $laneData->setModuleKanBanLane($lane);
        $laneData->setTitle($requestData->title);
        $laneData->setBody($requestData->body);
        $laneData->setRowOrder($requestData->rowOrder);
        $laneData->setUser($user);

        $em->persist($laneData);
        $em->flush();

        $this->pusherService->notification($pusherChannel, 'update', $laneData);

        return new JsonResponse(['message' => 'Lane data created.'], 200);
    }

    /**
     * @Route("/api/module/update/lane", name="update_module_kanban_lane", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function updateLanes(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $i = 0;

        $lanes = [];

        foreach ($requestData->lanes as $lane) {
            $targetLane = $em->getRepository(ModuleKanBanLane::class)->find($lane);
            $targetLane->setColOrder($i);

            $em->flush();

            $i++;

            $lanes[] = $targetLane;
        }

        $pusherChannel = 'module-' . $module->getId() . '-lane-drag';

        $this->pusherService->notification($pusherChannel, 'update', $lanes);

        return new JsonResponse(['message' => 'Lanes updated.'], 200);
    }

    /**
     * @Route("/api/module/update/lane/data", name="update_module_kanban_lane_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function updateLaneData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $sourceLane = $em->getRepository(ModuleKanBanLane::class)->find($requestData->sourceLaneId);

        if (empty($sourceLane)) {
            return new JsonResponse(['message' => 'The provided source lane does not exist.'], 404);
        }

        $targetLane = $em->getRepository(ModuleKanBanLane::class)->find($requestData->targetLaneId);

        if (empty($targetLane)) {
            return new JsonResponse(['message' => 'The provided target lane does not exist.'], 404);
        }

        $targetCard = $em->getRepository(ModuleKanBanData::class)->find($requestData->card);

        if (empty($targetCard)) {
            return new JsonResponse(['message' => 'The provided target card does not exist.'], 404);
        }

        $currentPosition = $targetCard->getRowOrder();

        $targetCard->setModuleKanBanLane($targetLane);
        $targetCard->setRowOrder($requestData->cardPosition);
        $em->flush();

        $a = 0;

        foreach ($em->getRepository(ModuleKanBanLane::class)->getLaneDataByOrder($sourceLane) as $sourceLaneData) {
            $sourceLaneData->setRowOrder($a);
            $a++;
        }

        $em->flush();

        $b = 0;

        foreach ($em->getRepository(ModuleKanBanLane::class)->getLaneDataByOrder($targetLane) as $targetLaneData) {
            if ($targetLaneData->getId() !== $targetCard->getId()) {
                if ($targetLaneData->getRowOrder() == $requestData->cardPosition) {
                    $b++;
                }
                $targetLaneData->setRowOrder($b);
                $b++;
            } else {
                continue;
            }
        }

        $em->flush();

        $pusherChannel = 'module-' . $module->getId() . '-card-drag';

        $responseData = [
            'card'         => [
                'id'       => (string) $targetCard->getId(),
                'title'    => $targetCard->getTitle(),
                'body'     => $targetCard->getBody(),
                'user'     => $targetCard->getUser()->getFirstName() . $targetCard->getUser()->getLastName(),
                'laneId'   => (string) $targetCard->getModuleKanBanLane()->getId()
            ],
            'targetLane'   => $requestData->targetLaneId,
            'sourceLane'   => $requestData->sourceLaneId,
            'cardPosition' => $requestData->cardPosition,
            'prevPosition' => $currentPosition
        ];

        $this->pusherService->notification($pusherChannel, 'update', $responseData);

        return new JsonResponse(['message' => 'Lane data updated.'], 200);
    }

    /**
     * @Route("/api/module/delete/lane/data", name="delete_module_kanban_lane_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function deleteLaneData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $targetCard = $em->getRepository(ModuleKanBanData::class)->find($requestData->card);

        if (empty($targetCard)) {
            return new JsonResponse(['message' => 'The provided card does not exist.'], 404);
        }

        $responseData = [
            'card' => (string) $targetCard->getId()
        ];

        $em->remove($targetCard);
        $em->flush();

        $pusherChannel = 'module-' . $module->getId() . '-card-delete';

        $this->pusherService->notification($pusherChannel, 'update', $responseData);

        return new JsonResponse(['message' => 'Card has been deleted.'], 200);
    }

    /**
     * @Route("/api/module/delete/lane", name="delete_module_kanban_lane", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function deleteLane(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $lane = $em->getRepository(ModuleKanBanLane::class)->find($requestData->lane);

        if (empty($lane)) {
            return new JsonResponse(['message' => 'The provided lane does not exist.'], 404);
        }

        $em->remove($lane);
        $em->flush();

        $pusherChannel = 'module-' . $module->getId() . '-lane-delete';

        $this->pusherService->notification($pusherChannel, 'update', $requestData->lane);

        return new JsonResponse(['message' => 'Lane has been deleted.'], 200);
    }

    /**
     * @Route("/api/module/delete", name="delete_module", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException|\Exception
     */
    public function deleteModule(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $user = $this->userService->getCurrentUser();

        $workSpace = $module->getWorkSpace();

        $workSpaceLog = new WorkSpaceLog();
        $workSpaceLog->setWorkSpace($workSpace);
        $workSpaceLog->setType("error");
        $workSpaceLog->setBody($user->getFirstName() . ' ' . $user->getLastName() . ' has deleted a ' . $module->getType() . ' module.');
        $workSpaceLog->setDate(new DateTime('now'));

        $em->persist($workSpaceLog);

        $pusherChannel = 'workspace-' . $workSpace->getId() . '-module-delete';

        $responseData = [
            'module'        => $module->getId(),
            'channel'       => !empty($module->getChannel()) ? $module->getChannel()->getId() : null,
            'channelGroup'  => !empty($module->getChannel()) && !empty($module->getChannel()->getChannelGroup()) ? $module->getChannel()->getChannelGroup()->getId() : null
        ];

        $em->remove($module);
        $em->flush();

        $this->pusherService->notification($pusherChannel, 'update', $responseData);

        return new JsonResponse(['message' => 'Module has been deleted.'], 200);
    }

    /**
     * @Route("/api/module/create/timer/data", name="create_module_timer_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException|\Exception
     */
    public function createTimeTrackerData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $module = $em->getRepository(Module::class)->find($requestData->module);

        if (empty($module)) {
            return new JsonResponse(['message' => 'The provided module does not exist.'], 404);
        }

        $user = $this->userService->getCurrentUser();

        $workSpaceUser = $em->getRepository(WorkSpaceUser::class)->findOneBy(['workSpace' => $module->getWorkSpace(), 'user' => $user]);

        $timeTrackerData = new ModuleTimeTrackData();
        $timeTrackerData->setUser($user);
        $timeTrackerData->setModule($module);
        $timeTrackerData->setTime($requestData->time);
        $timeTrackerData->setTitle($requestData->title);
        $timeTrackerData->setWorkSpaceUser($workSpaceUser);
        $timeTrackerData->setCreatedAt(new DateTime('now'));

        $em->persist($timeTrackerData);
        $em->flush();

        $pusherChannel = 'module-' . $module->getId() . '-timer';

        $responseData = [
            'time'  => $timeTrackerData->getTime(),
            'date'  => $timeTrackerData->getCreatedAt(),
            'title' => $timeTrackerData->getTitle(),
            'user'  => [
                'firstName' => $user->getFirstName(),
                'lastName'  => $user->getLastName()
            ]
        ];

        $this->pusherService->notification($pusherChannel, 'update', $responseData);

        $workSpaceDataChannel = 'workspace-' . $module->getWorkSpace()->getId() . '-data';

        $workSpaceData = [
            'target' => 'yearlyHours',
            'value'  => $timeTrackerData->getTime()
        ];

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        $workSpaceData = [
            'target' => 'monthlyHours',
            'value'  => $timeTrackerData->getTime()
        ];

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        return new JsonResponse(['message' => 'Time tracker data has been created.'], 200);
    }
}