<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserNotification;
use App\Entity\WorkSpace;
use App\Entity\WorkSpaceRole;
use App\Entity\WorkSpaceUser;
use App\Entity\WorkSpaceUserRole;
use App\Service\PusherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\MediaObject;
use App\Service\UserService;

class WorkSpaceController extends AbstractController
{
    protected $pusherService;
    protected $userService;

    public function __construct(PusherService $pusherService, UserService $userService)
    {
        $this->pusherService = $pusherService;
        $this->userService   = $userService;
    }

    /**
     * @Route("/api/workspace/create", name="workspace_create", methods={"POST"})
     */
    public function createWorkSpace(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $mediaObject = $em->getRepository(MediaObject::class)->find($requestData->image);

        if (empty($mediaObject)) {
            return new JsonResponse(['message' => 'The provided media object does not exist.'], 404);
        }

        $existing = $em->getRepository(WorkSpace::class)->findOneBy(['name' => $requestData->name]);

        if (!empty($existing)) {
            return new JsonResponse(['message' => 'The provided workspace name is already taken.'], 403);
        }

        $owner = $this->userService->getCurrentUser();

        $workSpace = new WorkSpace();
        $workSpace->setName($requestData->name);
        $workSpace->setImage($mediaObject);
        $workSpace->setOwner($owner);

        $em->persist($workSpace);

        $userRole = new WorkSpaceRole();
        $userRole->setRole("USER");
        $userRole->setWorkSpace($workSpace);
        $userRole->setRoleOrder(1);

        $em->persist($userRole);

        $adminRole = new WorkSpaceRole();
        $adminRole->setRole("ADMIN");
        $adminRole->setWorkSpace($workSpace);
        $adminRole->setRoleOrder(98);

        $em->persist($adminRole);

        $ownerRole = new WorkSpaceRole();
        $ownerRole->setRole("OWNER");
        $ownerRole->setWorkSpace($workSpace);
        $ownerRole->setRoleOrder(99);

        $em->persist($ownerRole);

        $workSpaceUser = new WorkSpaceUser();
        $workSpaceUser->setUser($owner);
        $workSpaceUser->setWorkSpace($workSpace);

        $em->persist($workSpaceUser);

        $workSpaceUserRole = new WorkSpaceUserRole();
        $workSpaceUserRole->setWorkSpaceRole($ownerRole);
        $workSpaceUserRole->setWorkSpaceUser($workSpaceUser);

        $em->persist($workSpaceUserRole);
        $em->flush();

        $data = [
            'image'         => [
                'filePath'  => $workSpace->getImage()->getFilePath()
            ],
            'id'            => $workSpace->getId(),
            'name'          => $workSpace->getName(),
            'owner'         => [
                '_id'       => $workSpace->getOwner()->getId()
            ],
            'workSpaceUser' => [
                'workSpace' => $workSpace->getId(),
                'roleOrder' => $ownerRole->getRoleOrder()
            ]
        ];

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/workspace/get/overview/data", name="workspace_get_overview_data", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getWorkSpaceOverviewData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $prevMonthlyHours   = $em->getRepository(WorkSpace::class)->getPrevMonthlyHours($workSpace);
        $monthlyHours       = $em->getRepository(WorkSpace::class)->getMonthlyHours($workSpace);
        $prevYearlyHours    = $em->getRepository(WorkSpace::class)->getPrevYearlyHours($workSpace);
        $yearlyHours        = $em->getRepository(WorkSpace::class)->getYearlyHours($workSpace);
        $userCount          = $em->getRepository(WorkSpace::class)->getUserCount($workSpace);
        $messageCount       = $em->getRepository(WorkSpace::class)->getMessageCount($workSpace);
        $groupCount         = $em->getRepository(WorkSpace::class)->getGroupCount($workSpace);
        $channelCount       = $em->getRepository(WorkSpace::class)->getChannelCount($workSpace);
        $moduleCount        = $em->getRepository(WorkSpace::class)->getModuleCount($workSpace);

        $responseData = [
            'data' => [
                'yearlyHours'      => (int)$yearlyHours,
                'prevYearlyHours'  => (int)$prevYearlyHours,
                'monthlyHours'     => (int)$monthlyHours,
                'prevMonthlyHours' => (int)$prevMonthlyHours,
                'userCount'        => (int)$userCount,
                'messageCount'     => (int)$messageCount,
                'groupCount'       => (int)$groupCount,
                'channelCount'     => (int)$channelCount,
                'moduleCount'      => (int)$moduleCount
            ],
            'logs' => $workSpace->getWorkSpaceLogsOrderedById()
        ];

        return new JsonResponse($responseData, 200);
    }

    /**
     * @Route("/api/workspace/create/role", name="workspace_create_role", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function createWorkSpaceRole(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $workSpaceRole = new WorkSpaceRole();
        $workSpaceRole->setWorkSpace($workSpace);
        $workSpaceRole->setRole($requestData->role);
        $workSpaceRole->setRoleOrder($requestData->roleOrder);

        $em->persist($workSpaceRole);
        $em->flush();

        $responseData = [
            '_id'       => $workSpaceRole->getId(),
            'role'      => $workSpaceRole->getRole(),
            'roleOrder' => $workSpaceRole->getRoleOrder()
        ];

        $pusherChannel = 'workspace-' . $workSpace->getId() . '-role';

        $this->pusherService->notification($pusherChannel, 'create', $responseData);

        return new JsonResponse('Role successfully created.', 200);
    }

    /**
     * @Route("/api/workspace/delete", name="workspace_delete", methods={"POST"})
     */
    public function deleteWorkSpace(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $workSpaceId = $workSpace->getId();

        $em->remove($workSpace);
        $em->flush();

        $pusherChannel = 'workspace-delete';
        $pusherInWorkSpaceChannel = 'workspace-' . $workSpaceId . '-delete';

        $this->pusherService->notification($pusherChannel, 'update', $workSpaceId);
        $this->pusherService->notification($pusherInWorkSpaceChannel, 'update', $workSpaceId);

        return new JsonResponse('Workspace successfully deleted.', 200);
    }

    /**
     * @Route("/api/workspace/get/user/hours", name="workspace_get_user_hours", methods={"POST"})
     */
    public function getUserHours(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $user = $em->getRepository(User::class)->find($requestData->user);

        if (empty($user)) {
            return new JsonResponse(['message' => 'The provided user does not exist.'], 404);
        }

        $monthlyHours     = $em->getRepository(User::class)->getMonthlyHours($workSpace, $user);
        $prevMonthlyHours = $em->getRepository(User::class)->getPrevMonthlyHours($workSpace, $user);

        $data = [
            "monthlyHours" => !empty($monthlyHours) ? (int) $monthlyHours : 0, 
            "prevMonthlyHours" => !empty($prevMonthlyHours) ? (int) $prevMonthlyHours : 0
        ];

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/workspace/delete/user", name="workspace_delete_user", methods={"POST"})
     */
    public function deleteWorkSpaceUser(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $user = $em->getRepository(User::class)->find($requestData->user);

        if (empty($user)) {
            return new JsonResponse(['message' => 'The provided user does not exist.'], 404);
        }

        $workSpaceUser = $em->getRepository(WorkSpaceUser::class)->findOneBy(['workSpace' => $workSpace, 'user' => $user]);

        $em->remove($workSpaceUser);
        $em->flush();

        $em->getRepository(User::class)->deleteChannelUsers($workSpace, $user);
        $em->getRepository(User::class)->deleteChannelGroupUsers($workSpace, $user);

        $pusherChannel = 'workspace-' . $workSpace->getId() . '-delete-user';

        $this->pusherService->notification($pusherChannel, 'delete', $user->getId());

        $privateChannel = 'user-' . $user->getId() . '-workspace-delete'; 

        $this->pusherService->notification($privateChannel, 'delete', $workSpace->getId());

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'userCount',
            'value'  => -1
        ]);

        $userNotification = new UserNotification();
        $userNotification->setUser($user);
        $userNotification->setType('notification');
        $userNotification->setBody("You've been removed from the " . $workSpace->getName() . " workspace.");

        $em->persist($userNotification);
        $em->flush();

        $pusherResponseData = [
            'body'          => $userNotification->getBody(),
            'inviteHandled' => $userNotification->getInviteHandled(),
            'handled'       => $userNotification->getHandled(),
            'type'          => $userNotification->getType(),
            'invite'        => null,
        ];

        $this->pusherService->notification($user->getEmail(), 'invite', $pusherResponseData);

        return new JsonResponse(['message' => 'The provided workspace user has been deleted.'], 200);
    }

    /**
     * @Route("/api/workspace/leave", name="workspace_leave", methods={"POST"})
     */
    public function leaveWorkSpace(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $user = $this->userService->getCurrentUser();

        $workSpaceUser = $em->getRepository(WorkSpaceUser::class)->findOneBy(['workSpace' => $workSpace, 'user' => $user]);

        $em->remove($workSpaceUser);
        $em->flush();

        $em->getRepository(User::class)->deleteChannelUsers($workSpace, $user);
        $em->getRepository(User::class)->deleteChannelGroupUsers($workSpace, $user);

        $pusherChannel = 'workspace-' . $workSpace->getId() . '-delete-user';

        $this->pusherService->notification($pusherChannel, 'delete', $user->getId());

        $privateChannel = 'user-' . $user->getId() . '-workspace-delete'; 

        $this->pusherService->notification($privateChannel, 'delete', $workSpace->getId());

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'userCount',
            'value'  => -1
        ]);

        return new JsonResponse(['message' => 'The provided workspace user has been deleted.'], 200);
    }
}