<?php

namespace App\Controller;

use DateTime;
use App\Entity\WorkSpaceUser;
use App\Service\PusherService;
use App\Service\UserService;
use App\Entity\Channel;
use App\Entity\ChannelGroup;
use App\Entity\ChannelGroupUser;
use App\Entity\ChannelUser;
use App\Entity\User;
use App\Entity\WorkSpace;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\WorkSpaceRole;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\WorkSpaceLog;

class ChannelController extends AbstractController
{
    protected $em;
    protected $pusherService;
    protected $userService;
    protected $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        PusherService $pusherService,
        UserService $userService,
        LoggerInterface $logger
    ) {
        $this->pusherService         = $pusherService;
        $this->em                    = $entityManager;
        $this->userService           = $userService;
        $this->logger                = $logger;
    }

    /**
     * @Route(path="/api/group/create", name="create_group", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function handleCreateGroup(Request $request)
    {
        $requestData = json_decode($request->getContent());

        $workSpace = $this->em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $workSpaceUsers = [];

        $channelGroup = new ChannelGroup();
        $channelGroup->setName($requestData->name);
        $channelGroup->setWorkSpace($workSpace);

        if (!empty($requestData->baseRole)) {
            $baseRole = $this->em->getRepository(WorkSpaceRole::class)->findOneBy(['id' => $requestData->baseRole, 'workSpace' => $workSpace]);
            $channelGroup->setBaseRole($baseRole);

            $workSpaceUsers = $this->em->getRepository(WorkSpaceUser::class)->getWorkSpaceUsers($workSpace, $baseRole->getRoleOrder());
            $workSpaceUsers = array_unique($workSpaceUsers, SORT_REGULAR);
        }

        $this->em->persist($channelGroup);
        $this->em->flush();

        $channelGroupUsers = [];

        foreach ($requestData->users as $userId) {
            $user = $this->em->getRepository(User::class)->find($userId);

            $channelGroupUser = new ChannelGroupUser();
            $channelGroupUser->setUser($user);
            $channelGroupUser->setChannelGroup($channelGroup);

            $this->em->persist($channelGroupUser);
            $this->em->flush();

            $channelGroupUsers[] = [
                'id' => $userId
            ];
        }

        $responseData = [
            "name"              => $channelGroup->getName(),
            "id"                => $channelGroup->getId(),
            "baseRole"          => [
                "id"            => $channelGroup->getBaseRole()->getId(),
                "role"          => $channelGroup->getBaseRole()->getRole(),
                "roleOrder"     => $channelGroup->getBaseRole()->getRoleOrder(),
            ],
            "channelGroupUsers" => $channelGroupUsers,
            "channels"          => []
        ];

        if (!empty($requestData->users)) {
            foreach ($requestData->users as $user) {
                $this->pusherService->notification('workspace-' . $workSpace->getId() . '-group-' . $user, 'create', $responseData);
            }
        } else {
            foreach ($workSpaceUsers as $user) {
                $this->pusherService->notification('workspace-' . $workSpace->getId() . '-group-' . $user["id"], 'create', $responseData);
            }
        }

        $workSpaceData = [
            'target' => 'groupCount',
            'value'  => 1
        ];

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        return new JsonResponse("Channel group successfully created.", 200);
    }

    /**
     * @Route(path="/api/channel/create", name="create_channel", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function handleCreateChannel(Request $request)
    {
        $requestData = json_decode($request->getContent());

        $workSpace  = $this->em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $workSpaceUsers = [];

        $channel = new Channel();
        $channel->setName($requestData->name);
        $channel->setWorkSpace($workSpace);

        if (!empty($requestData->baseRole)) {
            $baseRole = $this->em->getRepository(WorkSpaceRole::class)->findOneBy(['id' => $requestData->baseRole, 'workSpace' => $workSpace]);
            $channel->setBaseRole($baseRole);

            $workSpaceUsers = $this->em->getRepository(WorkSpaceUser::class)->getWorkSpaceUsers($workSpace, $baseRole->getRoleOrder());
            $workSpaceUsers = array_unique($workSpaceUsers, SORT_REGULAR);
        }

        $this->em->persist($channel);
        $this->em->flush();

        $channelUsers = [];

        foreach ($requestData->users as $userId) {
            $user = $this->em->getRepository(User::class)->find($userId);

            $channelUser = new ChannelUser();
            $channelUser->setUser($user);
            $channelUser->setChannel($channel);

            $this->em->persist($channelUser);
            $this->em->flush();

            $channelUsers[] = [
                'id' => $userId
            ];
        }

        $responseData = [
            "name"          => $channel->getName(),
            "id"            => $channel->getId(),
            "baseRole"      => [
                "id"        => $channel->getBaseRole()->getId(),
                "role"      => $channel->getBaseRole()->getRole(),
                "roleOrder" => $channel->getBaseRole()->getRoleOrder(),
            ],
            "channelUser"   => $channelUsers,
            "modules"       => []
        ];

        if (!empty($requestData->users)) {
            foreach ($requestData->users as $user) {
                $this->pusherService->notification('workspace-' . $workSpace->getId() . '-channel-' . $user, 'create', $responseData);
            }
        } else {
            foreach ($workSpaceUsers as $user) {
                $this->pusherService->notification('workspace-' . $workSpace->getId() . '-channel-' . $user["id"], 'create', $responseData);
            }
        }

        $workSpaceData = [
            'target' => 'channelCount',
            'value'  => 1
        ];

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        return new JsonResponse("Channel successfully created.", 200);
    }

    /**
     * @Route(path="/api/channel/add/users", name="channel_add_user", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function handleChannelCreateUser(Request $request)
    {
        $requestData = json_decode($request->getContent());

        $workSpace = $this->em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $channel = $this->em->getRepository(Channel::class)->find($requestData->channel);

        if (empty($channel)) {
            return new JsonResponse(['message' => 'The provided channel does not exist.'], 404);
        }

        foreach ($requestData->users as $userId) {
            $user = $this->em->getRepository(User::class)->find($userId);

            $channelUser = new ChannelUser();
            $channelUser->setUser($user);
            $channelUser->setChannel($channel);

            $this->em->persist($channelUser);
        }

        $this->em->flush();

        $channelUsers = $this->em->getRepository(Channel::class)->getChannelUsers($channel, $workSpace);

        $responseData = [
            'id'                => $channel->getId(),
            'name'              => $channel->getName(),
            'group'             => !empty($channel->getChannelGroup()) ? $channel->getChannelGroup()->getId() : null,
            'channelUser'       => $channelUsers,
            'modules'           => [],
            'baseRole'          => [
                'id'            => $channel->getBaseRole()->getId(),
                'role'          => $channel->getBaseRole()->getRole(),
                'roleOrder'     => $channel->getBaseRole()->getRoleOrder()
            ]
        ];

        foreach ($channel->getModules() as $module) {
            $responseData['modules'][] = [
                'id'        => $module->getId(),
                'type'      => $module->getType(),
                'name'      => $module->getName(),
                'channel'   => [
                    'id'    => $channel->getId(),
                    'name'  => $channel->getName()
                ]
            ];
        };


        foreach ($channelUsers as $user) {
            $this->pusherService->notification('user-' . $user["id"] . '-new-channel', 'create', $responseData);
        }

        return new JsonResponse(['message' => 'User(s) successfully added to the group.'], 200);
    }

    /**
     * @Route("/api/group/add/users", name="group_add_users", methods={"POST"})
     */
    public function handleGroupCreateUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $group = $em->getRepository(ChannelGroup::class)->find($requestData->channelGroup);

        if (empty($group)) {
            return new JsonResponse(['message' => 'The provided channel group does not exist.'], 404);
        }

        foreach ($requestData->users as $user) {
            $groupUser = $em->getRepository(User::class)->find($user);

            if ($groupUser) {
                $channelGroupUser = new ChannelGroupUser();
                $channelGroupUser->setUser($groupUser);
                $channelGroupUser->setChannelGroup($group);

                $em->persist($channelGroupUser);
            }
        }

        $em->flush();

        $groupUsers = $em->getRepository(ChannelGroup::class)->getChannelGroupUsers($group, $workSpace);

        $responseData = [
            'id'                => $group->getId(),
            'name'              => $group->getName(),
            'channelGroupUsers' => $groupUsers,
            'newUsers'          => $requestData->users,
            'channels'          => [],
            'baseRole'          => [
                'id'            => $group->getBaseRole()->getId(),
                'role'          => $group->getBaseRole()->getRole(),
                'roleOrder'     => $group->getBaseRole()->getRoleOrder()
            ]
        ];

        foreach ($group->getChannel() as $channel) {
            $data = [
                'id'                => $channel->getId(),
                'name'              => $channel->getName(),
                'group'             => $group->getId(),
                'channelUser'       => $em->getRepository(Channel::class)->getChannelUsers($channel, $workSpace),
                'modules'           => [],
                'baseRole'          => [
                    'id'            => $channel->getBaseRole()->getId(),
                    'role'          => $channel->getBaseRole()->getRole(),
                    'roleOrder'     => $channel->getBaseRole()->getRoleOrder()
                ]
            ];

            foreach ($channel->getModules() as $module) {
                $data['modules'][] = [
                    'id'        => $module->getId(),
                    'type'      => $module->getType(),
                    'name'      => $module->getName(),
                    'channel'   => [
                        'id'    => $channel->getId(),
                        'name'  => $channel->getName()
                    ]
                ];
            };

            $responseData['channels'][] = $data;
        };

        foreach ($groupUsers as $user) {
            $this->pusherService->notification('user-' . $user["id"] . '-new-group', 'create', $responseData);
        }

        return new JsonResponse(['message' => 'User(s) successfully added to the group.'], 200);
    }

    /**
     * @Route("/api/user/get/channels", name="get_user_channels", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserChannels(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $userData = $em->getRepository(User::class)->getUserWorkSpaceData($workSpace);

        return new JsonResponse($userData, 200);
    }

    /**
     * @Route("/api/channel/get/users", name="get_channel_users", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getUsers(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        if (!empty($requestData->channelGroup)) {
            $channelGroup = $em->getRepository(ChannelGroup::class)->find($requestData->channelGroup);

            if (empty($channelGroup)) {
                return new JsonResponse(['message' => 'The provided channel group does not exist.'], 404);
            }

            $channelGroupUsers = $em->getRepository(ChannelGroup::class)->getChannelGroupUsers($channelGroup, $workSpace);

            return new JsonResponse($channelGroupUsers, 200);
        }

        if (!empty($requestData->channel)) {
            $channel = $em->getRepository(Channel::class)->find($requestData->channel);

            if (empty($channel)) {
                return new JsonResponse(['message' => 'The provided channel does not exist.'], 404);
            }

            $channelUsers = $em->getRepository(Channel::class)->getChannelUsers($channel, $workSpace);

            return new JsonResponse($channelUsers, 200);
        }
    }

    /**
     * @Route("/api/group/create/channel", name="group_create_channel", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function handleGroupCreateChannel(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $group = $em->getRepository(ChannelGroup::class)->find($requestData->channelGroup);

        if (empty($group)) {
            return new JsonResponse(['message' => 'The provided channel group does not exist.'], 404);
        }

        $workSpaceUsers = [];

        $channel = new Channel();
        $channel->setName($requestData->name);
        $channel->setWorkSpace($workSpace);
        $channel->setChannelGroup($group);

        if (!empty($requestData->baseRole)) {
            $baseRole = $this->em->getRepository(WorkSpaceRole::class)->findOneBy(['id' => $requestData->baseRole, 'workSpace' => $workSpace]);
            $channel->setBaseRole($baseRole);

            $workSpaceUsers = $this->em->getRepository(WorkSpaceUser::class)->getWorkSpaceUsers($workSpace, $baseRole->getRoleOrder());
            $workSpaceUsers = array_unique($workSpaceUsers, SORT_REGULAR);
        }

        $this->em->persist($channel);
        $this->em->flush();

        $channelUsers = [];

        foreach ($requestData->users as $userId) {
            $user = $this->em->getRepository(User::class)->find($userId);

            $channelUser = new ChannelUser();
            $channelUser->setUser($user);
            $channelUser->setChannel($channel);

            $this->em->persist($channelUser);
            $this->em->flush();

            $channelUsers[] = $userId;
        }

        $responseData = [
            "group"         => $group->getId(),
            "name"          => $channel->getName(),
            "id"            => $channel->getId(),
            "baseRole"      => [
                "id"        => $channel->getBaseRole()->getId(),
                "role"      => $channel->getBaseRole()->getRole(),
                "roleOrder" => $channel->getBaseRole()->getRoleOrder(),
            ],
            "channelUser"   => $channelUsers,
            "modules"       => []
        ];

        if (!empty($requestData->users)) {
            foreach ($requestData->users as $user) {
                $this->pusherService->notification('workspace-' . $workSpace->getId() . '-group-channel-' . $user, 'create', $responseData);
            }
        } else {
            foreach ($workSpaceUsers as $user) {
                $this->pusherService->notification('workspace-' . $workSpace->getId() . '-group-channel-' . $user["id"], 'create', $responseData);
            }
        }

        $workSpaceData = [
            'target' => 'channelCount',
            'value'  => 1
        ];

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', $workSpaceData);

        return new JsonResponse("Channel successfully created.", 200);
    }

    /**
     * @Route("/api/workspace/delete/group", name="workspace_delete_group", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function deleteGroup(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        $user = $this->userService->getCurrentUser();

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $group = $em->getRepository(ChannelGroup::class)->find($requestData->channelGroup);

        if (empty($group)) {
            return new JsonResponse(['message' => 'The provided channel group does not exist.'], 404);
        }

        $groupData = [
            'channelGroup'  => $group->getId(),
            'channels'      => [],
            'modules'       => []
        ];

        foreach ($group->getChannel() as $channel) {
            $groupData['channels'][] = $channel->getId();
            foreach ($channel->getModules() as $module) {
                $groupData['modules'][] = $module->getId();
            }
        }

        $workSpaceLog = new WorkSpaceLog();
        $workSpaceLog->setBody($user->getFirstName() . " " . $user->getLastName() . " has deleted the channel group " . $group->getName());
        $workSpaceLog->setDate(new DateTime('now'));
        $workSpaceLog->setWorkSpace($workSpace);
        $workSpaceLog->setType("error");

        $em->persist($workSpaceLog);

        $em->remove($group);
        $em->flush();

        $workspaceDeleteGroupChannel = 'workspace-' . $workSpace->getId() . '-delete-group';

        $this->pusherService->notification($workspaceDeleteGroupChannel, 'delete', $groupData);

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'groupCount',
            'value'  => -1
        ]);

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'channelCount',
            'value'  => count($groupData['channels']) * -1
        ]);

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'moduleCount',
            'value'  => count($groupData['modules']) * -1
        ]);

        return new JsonResponse("Group successfully deleted.", 200);
    }

    /**
     * @Route("/api/workspace/delete/channel", name="workspace_delete_channel", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function deleteChannel(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        $user = $this->userService->getCurrentUser();

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $channel = $em->getRepository(Channel::class)->find($requestData->channel);

        if (empty($channel)) {
            return new JsonResponse(['message' => 'The provided channel does not exist.'], 404);
        }

        $channelData = [
            'channel'      => $channel->getId(),
            'channelGroup' => !empty($channel->getChannelGroup()) ? $channel->getChannelGroup()->getId() : null,
            'modules'      => []
        ];

        foreach ($channel->getModules() as $module) {
            $channelData['modules'][] = $module->getId();
        }

        $workSpaceLog = new WorkSpaceLog();
        $workSpaceLog->setBody($user->getFirstName() . " " . $user->getLastName() . " has deleted the channel " . $channel->getName());
        $workSpaceLog->setDate(new DateTime('now'));
        $workSpaceLog->setWorkSpace($workSpace);
        $workSpaceLog->setType("error");

        $em->persist($workSpaceLog);

        $em->remove($channel);
        $em->flush();

        $workspaceDeleteChannel = 'workspace-' . $workSpace->getId() . '-delete-channel';

        $this->pusherService->notification($workspaceDeleteChannel, 'delete', $channelData);

        $workSpaceDataChannel = 'workspace-' . $workSpace->getId() . '-data';

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'channelCount',
            'value'  => -1
        ]);

        $this->pusherService->notification($workSpaceDataChannel, 'update', [
            'target' => 'moduleCount',
            'value'  => count($channelData['modules']) * -1
        ]);

        return new JsonResponse("Channel successfully deleted.", 200);
    }

    /**
     * @Route("/api/group/delete/user", name="group_delete_user", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function deleteUserFromGroup(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $group = $em->getRepository(ChannelGroup::class)->find($requestData->channelGroup);

        if (empty($group)) {
            return new JsonResponse(['message' => 'The provided channel group does not exist.'], 404);
        }

        $channelGroupUser = $em->getRepository(ChannelGroupUser::class)->findOneBy(['user' => $requestData->user, 'channelGroup' => $group]);

        if (empty($channelGroupUser)) {
            return new JsonResponse(['message' => 'The provided channel group user does not exist.'], 404);
        }

        $responseData = [
            'channelGroup' => $group->getId(),
            'channels'     => [],
            'modules'      => []
        ];

        foreach ($group->getChannel() as $channel) {
            $responseData['channels'][] = $channel->getId(); 
            foreach ($channel->getModules() as $module) {
                $responseData['modules'][] = $module->getId(); 
            }
        }

        $em->remove($channelGroupUser);
        $em->flush();

        $userDeleteChannel = 'user-' . $requestData->user . '-delete-group';

        $this->pusherService->notification($userDeleteChannel, 'delete', $responseData);

        return new JsonResponse("Channel group user successfully deleted.", 200);
    }

    /**
     * @Route("/api/channel/delete/user", name="channel_delete_user", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Pusher\PusherException
     */
    public function deleteUserFromChannel(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $workSpace = $em->getRepository(WorkSpace::class)->find($requestData->workSpace);

        if (empty($workSpace)) {
            return new JsonResponse(['message' => 'The provided workspace does not exist.'], 404);
        }

        $channel = $em->getRepository(Channel::class)->find($requestData->channel);

        if (empty($channel)) {
            return new JsonResponse(['message' => 'The provided channel does not exist.'], 404);
        }

        $channelUser = $em->getRepository(ChannelUser::class)->findOneBy(['user' => $requestData->user, 'channel' => $channel]);

        if (empty($channelUser)) {
            return new JsonResponse(['message' => 'The provided channel user does not exist.'], 404);
        }

        $responseData = [
            'channel'      => $channel->getId(),
            'channelGroup' => !empty($channel->getChannelGroup()) ? $channel->getChannelGroup()->getId() : null,
            'modules'      => []
        ];

        foreach ($channel->getModules() as $module) {
            $responseData['modules'][] = $module->getId();
        }

        $em->remove($channelUser);
        $em->flush();

        $userDeleteChannel = 'user-' . $requestData->user . '-delete-channel';

        $this->pusherService->notification($userDeleteChannel, 'delete', $responseData);

        return new JsonResponse("Channel group user successfully deleted.", 200);
    }
}
