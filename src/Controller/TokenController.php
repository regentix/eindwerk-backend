<?php

namespace App\Controller;

use App\Entity\ConfirmationToken;
use App\Entity\ResetToken;
use App\Entity\User;
use App\Service\MailService;
use App\Service\TokenService;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TokenController extends AbstractController
{
    protected $mailService;
    protected $tokenService;

    public function __construct(MailService $mailService, TokenService $tokenService)
    {
        $this->mailService = $mailService;
        $this->tokenService = $tokenService;
    }

    /**
     * @Route("/api/user/verify-reset-token", name="verify_reset_token", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function verifyResetToken(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $requestData = json_decode($request->getContent());

        $user = $em->getRepository(User::class)->findOneBy(['email' => $requestData->email]);

        if (!$user instanceof User) {
            return new JsonResponse(['message' => 'No user found for the provided email.'], 404);
        }

        $userResetToken = $user->getResetToken();

        if (!$userResetToken instanceof ResetToken) {
            return new JsonResponse(['message' => 'The user did not request to reset password.'], 403);
        }

        $isValid = $this->tokenService->validateToken($userResetToken, $requestData->token);

        if (!$isValid) {
            return new JsonResponse(['message' => 'The reset token is invalid.'], 403);
        }

        $token = $this->tokenService->generateResetToken(12);

        $expiresAt = new Carbon();
        $expiresAt->addMinutes(5);

        if (empty($user->getResetToken()->getConfirmationToken())) {
            $confirmationToken = new ConfirmationToken();
            $confirmationToken->setToken($token);
            $confirmationToken->setExpiresAt($expiresAt);
            $confirmationToken->setResetToken($userResetToken);

            $em->persist($confirmationToken);

        } else {
            $user->getResetToken()->getConfirmationToken()->setToken($token);
            $user->getResetToken()->getConfirmationToken()->setExpiresAt($expiresAt);
        }

        $em->flush();

        $responseData = [
            'confirmationToken'   => $token
        ];

        return new JsonResponse($responseData, 200);
    }
}