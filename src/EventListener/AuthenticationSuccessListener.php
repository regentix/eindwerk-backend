<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User;

class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        if ($user instanceof User) {

            $workSpaceUsers = [];

            foreach ($user->getWorkSpaceUsers() as $workSpaceUser) {
                $workSpaceUsers[] = [
                    "workSpace" => $workSpaceUser->getWorkSpace()->getId(),
                    "roleOrder" => $workSpaceUser->getWorkSpaceUserRole()->getWorkSpaceRole()->getRoleOrder()
                ];
            }

            $data['user'] = [
                'firstName'     => $user->getFirstName(),
                'lastName'      => $user->getLastName(),
                'email'         => $user->getEmail(),
                'status'        => $user->getStatus(),
                'id'            => $user->getId(),
                'workSpaceUser' => $workSpaceUsers
            ];
        }

        $event->setData($data);
    }
}