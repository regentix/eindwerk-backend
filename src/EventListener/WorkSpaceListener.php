<?php

namespace App\EventListener;

use App\Entity\Channel;
use App\Entity\ChannelGroup;
use App\Entity\WorkSpaceLog;
use App\Service\PusherService;
use App\Service\UserService;
use DateTime;
use App\Entity\User;
use App\Entity\WorkSpace;
use App\Entity\WorkSpaceInvite;
use App\Entity\WorkSpaceUser;
use App\Entity\ModuleKanBanData;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use App\Entity\UserNotification;
use App\Entity\ModuleChatData;

class WorkSpaceListener
{
    protected $userService;
    protected $logger;
    protected $pusherService;

    public function __construct(UserService $userService, LoggerInterface $logger, PusherService $pusherService)
    {
        $this->userService   = $userService;
        $this->logger        = $logger;
        $this->pusherService = $pusherService;
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (
            $entity instanceof WorkSpace        ||
            $entity instanceof WorkSpaceInvite  ||
            $entity instanceof WorkSpaceUser    ||
            $entity instanceof ChannelGroup     ||
            $entity instanceof Channel
        ) {
            $this->createWorkSpaceLog($args, $entity);
        }

        if ($entity instanceof WorkSpaceLog) {
            $this->dispatchLogEvent($entity);
        }

        if ($entity instanceof ModuleKanBanData) {
            $this->dispatchKanBanEvent($entity);
        }

        if ($entity instanceof ModuleChatData) {
            $this->dispatchChatEvent($entity, $args);
        }
    }

    /**
     * @param $args
     * @param $entity
     * @param $user
     * @return WorkSpaceUser
     */
    public function createWorkSpaceUser($args, $entity, $user)
    {
        $entityManager = $args->getObjectManager();

        $workSpaceUser = new WorkSpaceUser();
        $workSpaceUser->setUser($user);
        $workSpaceUser->setWorkSpace($entity);

        $entityManager->persist($workSpaceUser);
        $entityManager->flush();

        return $workSpaceUser;
    }

    /**
     * @param $args
     * @param $entity
     * @throws \Exception
     */
    public function createWorkSpaceLog($args, $entity)
    {
        $entityManager = $args->getObjectManager();

        $workSpaceLog = new WorkSpaceLog();
        $workSpaceLog->setDate(new DateTime('now'));

        if ($entity instanceof WorkSpace) {
            $workSpaceLog->setWorkSpace($entity);
            $workSpaceLog->setType('info');
            $workSpaceLog->setBody($entity->getOwner()->getFirstName() . " " . $entity->getOwner()->getLastName()  . " created the workspace.");
        }

        if ($entity instanceof WorkSpaceUser) {
            $workSpaceLog->setWorkSpace($entity->getWorkSpace());
            $workSpaceLog->setType('success');
            $workSpaceLog->setBody($entity->getUser()->getFirstName() . " " . $entity->getUser()->getLastName() . " joined the workspace.");
        }

        if ($entity instanceof WorkSpaceInvite) {
            $workSpaceLog->setWorkSpace($entity->getWorkSpace());
            $workSpaceLog->setType('info');
            $workSpaceLog->setBody($entity->getSentBy()->getFirstName() . " " . $entity->getSentBy()->getLastName() . " invited " .  $entity->getTarget()->getFirstName() . " " . $entity->getTarget()->getLastName() . " to join the workspace.");
        }

        if ($entity instanceof ChannelGroup) {
            $workSpaceLog->setWorkSpace($entity->getWorkSpace());
            $workSpaceLog->setType('info');
            $workSpaceLog->setBody($this->userService->getCurrentUser()->getFirstName() . " " . $this->userService->getCurrentUser()->getLastName() . " created a channel group named " . $entity->getName() . ".");
        }

        if ($entity instanceof Channel) {
            $workSpaceLog->setWorkSpace($entity->getWorkSpace());
            $workSpaceLog->setType('info');
            $workSpaceLog->setBody($this->userService->getCurrentUser()->getFirstName() . " " . $this->userService->getCurrentUser()->getLastName() . " created a channel named " . $entity->getName() . ".");
        }

        $entityManager->persist($workSpaceLog);
        $entityManager->flush();
    }

    /**
     * @param $entity
     * @throws \Pusher\PusherException
     */
    public function dispatchLogEvent($entity)
    {
        $channel = 'workspace-' . $entity->getWorkSpace()->getId() . '-history';

        $data = [
            'body' => $entity->getBody(),
            'type' => $entity->getType(),
            'date' => $entity->getDate()
        ];

        $this->pusherService->notification($channel, 'update', $data);
    }

    public function dispatchKanBanEvent($entity)
    {
        $user = $this->userService->getCurrentUser();
        $sentBy = $user->getFirstName() . ' ' . $user->getLastName();
        $module = $entity->getModuleKanBanLane()->getModule()->getName();
        $workSpace = $entity->getModuleKanBanLane()->getModule()->getWorkSpace()->getName();

        $notification = new UserNotification();
        $notification->setBody($sentBy . ' has assigned you to a task in ' . $workSpace . ' - ' . $module);
        $notification->setUser($entity->getUser());
        $notification->setType('notification');

        $data = [
            'body' => $notification->getBody(),
            'type' => $notification->getType(),
            'date' => new DateTime('now'),
            'info' => true
        ];

        $this->pusherService->notification($entity->getUser()->getEmail(), 'notification', $data);
    }

    public function dispatchChatEvent($entity, $args)
    {
        $entityManager = $args->getObjectManager();
        $user = $this->userService->getCurrentUser();
        $sentBy = $user->getFirstName() . ' ' . $user->getLastName();
        $module = $entity->getModule()->getName();
        $workSpace = $entity->getModule()->getWorkSpace()->getName();

        preg_match_all("/[@]([\w\.]+)/", $entity->getBody(), $tags);

        foreach ($tags[0] as $tag) {
            $taggedUser = $entityManager->getRepository(User::class)->findOneBy(['tag' => $tag]);

            if ($taggedUser && $taggedUser->getId() !== $user->getId()) {
                $notification = new UserNotification();
                $notification->setBody($sentBy . ' has mentioned your name in ' . $workSpace . ' - ' . $module);
                $notification->setUser($taggedUser);
                $notification->setType('notification');
        
                $data = [
                    'body' => $notification->getBody(),
                    'type' => $notification->getType(),
                    'date' => new DateTime('now'),
                    'info' => true
                ];
        
                $this->pusherService->notification($taggedUser->getEmail(), 'notification', $data);
            }
        }
    }
}
